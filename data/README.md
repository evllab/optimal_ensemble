Data
=
All Data to be processed will be stored here in local repositories, but never pushed to the public remote repository. This exists to help enforce a standard for where to retrieve data.
