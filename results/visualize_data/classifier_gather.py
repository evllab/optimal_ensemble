"""
get top 10 classifiers for best individual and mrmr

:author: Derek Prijatelj
"""

import sys
import os
from datetime import datetime
import subprocess
import pandas as pd
import numpy as np
from sklearn.metrics import accuracy_score
from average_sets import recursive_gather

def best_individual_feature_selection(train, feature_count):
    accuracy = [accuracy_score(train['True Author'], train[x]) for x in
    train.iloc[:,2:]]
    sorted_acc, original_idx = zip(*sorted(zip(accuracy,range(len(accuracy)))))
    return list(
        train.iloc[:,np.asarray(original_idx[:feature_count]) + 2].columns)

def mRMR_feature_selection(train, feature_count):
    # TODO create temporary or pipe train data csv to Rscript!

    # create temporary csv to use for this
    tmp_name = '__temporary_train_mrmr_csv__' + str(datetime.now()) + '.csv'

    # check if file already exists, if so, append incremental number on it
    if os.path.exists(tmp_name):
        idx = tmp_name.rfind(';')
        value = 1 + int(tmp[idx + 1:-4]) if idx != -1 else 1

        if idx == -1:
            idx = -4
        tmp_name = tmp_name[:idx] + ';' + str(value) + '.csv'

    train.to_csv(tmp_name, index=False)

    print('mRMR train\n', train)

    # call Rscript with tmp csv
    output = subprocess.check_output([
    'Rscript',
    '--vanilla',
    '../../src/stats/mRMRe.r',
    tmp_name,
    str(feature_count)
    ]).strip().decode()

    # delete temporary csv
    os.remove(tmp_name)

    return output.split('"')[1::2]

def get_top_k(current_dir):
    csv_dirs = recursive_gather(current_dir)

    enums = [(p, pd.read_csv(p, compression='gzip')) for p in csv_dirs]

    classifiers_df = []
    for p, e in enums:
        best = [b[5:b.rfind(';')] for b in best_individual_feature_selection(e, 10)]
        mrmr = [m[5:m.rfind(';')] for m in mRMR_feature_selection(e, 10)]
        task_set = p[p.rfind('_') + 1:-7]

        classifiers_df.append(pd.DataFrame(
            np.transpose([[task_set] * 10, list(range(1,11)), best, mrmr]),
            columns=['Task Set', 'Rank', 'Best Individual', 'mRMR']
        ))

    concat = pd.concat(classifiers_df)
    concat.to_csv("top10_classifiers.csv", index=False)

if __name__ == '__main__':
    get_top_k(sys.argv[1])
