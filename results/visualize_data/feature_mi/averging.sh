#!/bin/bash

ENUM_DIR="../../../data/enum/tri-test-36_features-greek_english/"

GROUP="monolingual"

TASK_SET="en1en2"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz

TASK_SET="en2en1"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz

TASK_SET="gr1gr2"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz

TASK_SET="gr2gr1"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz


GROUP="translation"

TASK_SET="en1gr1"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz

TASK_SET="en2gr2"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz

TASK_SET="gr1en1"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz

TASK_SET="gr2en2"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz

GROUP="nontranslation"

TASK_SET="en1gr2"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz

TASK_SET="en2gr1"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz

TASK_SET="gr1en2"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz

TASK_SET="gr2en1"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz
