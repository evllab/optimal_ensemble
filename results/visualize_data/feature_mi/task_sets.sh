#!/bin/bash

ENUM_DIR="../../../data/enum/tri-test-36_features-greek_english/"

GROUP="monolingual"
mkdir $GROUP

TASK_SET="en1en2"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz
Rscript mi_mat.R "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv $GROUP/$TASK_SET.png $TASK_SET 
rm "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv

TASK_SET="en2en1"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz
Rscript mi_mat.R "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv $GROUP/$TASK_SET.png $TASK_SET
rm "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv

TASK_SET="gr1gr2"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz
Rscript mi_mat.R "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv $GROUP/$TASK_SET.png $TASK_SET
rm "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv

TASK_SET="gr2gr1"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz
Rscript mi_mat.R "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv $GROUP/$TASK_SET.png $TASK_SET
rm "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv


GROUP="translation"
mkdir $GROUP

TASK_SET="en1gr1"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz
Rscript mi_mat.R "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv $GROUP/$TASK_SET.png $TASK_SET 
rm "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv

TASK_SET="en2gr2"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz
Rscript mi_mat.R "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv $GROUP/$TASK_SET.png $TASK_SET
rm "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv

TASK_SET="gr1en1"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz
Rscript mi_mat.R "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv $GROUP/$TASK_SET.png $TASK_SET
rm "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv

TASK_SET="gr2en2"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz
Rscript mi_mat.R "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv $GROUP/$TASK_SET.png $TASK_SET
rm "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv

GROUP="nontranslation"
mkdir $GROUP

TASK_SET="en1gr2"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz
Rscript mi_mat.R "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv $GROUP/$TASK_SET.png $TASK_SET 
rm "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv

TASK_SET="en2gr1"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz
Rscript mi_mat.R "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv $GROUP/$TASK_SET.png $TASK_SET
rm "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv

TASK_SET="gr1en2"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz
Rscript mi_mat.R "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv $GROUP/$TASK_SET.png $TASK_SET
rm "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv

TASK_SET="gr2en1"
gunzip -k "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv.gz
Rscript mi_mat.R "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv $GROUP/$TASK_SET.png $TASK_SET
rm "$ENUM_DIR"$GROUP/enum_$TASK_SET.csv
