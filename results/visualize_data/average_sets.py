"""
Average the test results for each 12 sets, then average the 3 groups
:author: Derek S. Prijatelj
"""

import sys
import os
import pandas as pd
import numpy as np
import argparse

def recursive_gather(current_dir):
    """ gather all result csv.gzs and store in list. """
    dir_list = os.listdir(current_dir)
    csvs = [os.path.join(current_dir, f) for f in dir_list
        if os.path.isfile(os.path.join(current_dir, f)) and '.csv.gz' == f[-7:]]

    for d in dir_list:
        if os.path.isdir(os.path.join(current_dir, d)):
            csvs += recursive_gather(os.path.join(current_dir, d))
    return csvs

def get_averages(current_dir):
    # get file paths
    result_paths = recursive_gather(current_dir)

    # open dataframes, save as {file_path: dataframe}
    results = {p : pd.read_csv(p, compression="gzip") for p in result_paths}

    indices = ['Group', 'Task Set', 'Nested N', 'K-Folds CV', 'Unnamed: 0',
        'Unnamed: 1', 'Feature Count']

    # remove all eval rows, and eval/test column after
    # average all class results per dataframe
    for p, df in results.items():
        df.drop(df[df.iloc[:,2]=="eval"].index, inplace=True)
        df.drop(df.columns.tolist()[2], 'columns', inplace=True)
        df = df.groupby(['Unnamed: 0', 'Unnamed: 1']).mean()
        df.reset_index(inplace=True)

        # create mutli-indexed dataframe, ready for concatenation.
        task_set, n, k, f , e = p.split(os.sep)[-2].split('-')
        if task_set[0:2] == task_set[3:5]:
            group = "monolingual"
        elif task_set[2] == task_set[5]:
            group = "translation"
        else:
            group = "nontranslation"
        df['Group'] = group
        df['Task Set'] = task_set
        df['Nested N'] = int(n[1:])
        df['K-Folds CV'] = int(k[1:])
        df['Feature Count'] = [int(f[1:])] * len(df.index)
        df.set_index(indices, append=True, inplace=True)

        # save modified dataframe
        results[p] = df

    # Concatenate the results, save.
    concat = pd.concat(list(results.values()))
    concat.reset_index(inplace=True)
    concat.drop(['level_0'], 'columns', inplace=True)
    concat.rename(columns={'Unnamed: 0':'Ensemble','Unnamed: 1':'Feature Selection'},
         inplace=True)
    concat.to_csv("Concat_Avg12.csv", index=False)

    indices[4] = 'Ensemble'
    indices[5] = 'Feature Selection'

    # Then average by 3 groups, save.
    concat = concat.groupby(indices[0:1] + indices[2:]).mean()
    concat.reset_index(inplace=True)
    concat.to_csv("Concat_Avg_3_groups.csv", index=False)

    # Then average by everything but
    concat = concat.groupby(indices[2:]).mean()
    concat.reset_index(inplace=True)
    concat.to_csv("Concat_Avg_Overall.csv", index=False)

if __name__ == "__main__":
    get_averages(sys.argv[1])
