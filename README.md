#Automated Construction of an Optimal Ensemble

An automated pipeline for finding the optimal ensemble for the authorship attribution process.

Research for optimal ensemble for given data sets.
Code of the project that corresponds with the presentation "Towards Cross-Language Automated Authorship Attribution” at the 2018 Keystone DH Conference at Penn State University.

##Dependencies
- Java
    + JGAAP.jar
- Python 3+
    - numpy
    - pandas
    - scikit-learn
- R
- bash for helper scripts
