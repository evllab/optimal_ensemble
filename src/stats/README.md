Stats
==

This is the stats focused scripts for analyzing the statistics behind JGAAP experiments.

Conditional Probability Matrix
--
Given a directory with the csv's of individial tests to be included in this experiment analysis and an output path for the resulting conditional independence matrix, this script creates the binary tally for each test under their respective linguistic analysis method. The binary tally is then used to create the conditional probability matrix between all analysis methods. Each matrix cell corresponds to P(row, col), so the method in the row's conditional probability given the column method is in that cell.
