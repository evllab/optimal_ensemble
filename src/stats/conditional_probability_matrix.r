#!/usr/bin/env Rscript
# R function and helper functions for creating a NxN matrix of the Linguistic
# Analysis Methods that contains the |P(B|A) - P(B|-A)|, where B is the col
# method and A is the row method.
#
# @author Derek S. Prijatelj

# Constructs the NxN matrix of |P(B|A) - P(B|-A)|, B is col, A is row, from the
# given matrix where each row is a different random variable with columns being
# 1 for success, 0 for false.
#
# @param binary_pmat: Matrix with rows different R.V.s, columns binary instances
#   of success(1) or failure(0)

# TODO need to add CLI usage to complete the script!

library("data.table") # For speed and optimizations over data.frame

#global var
binary_success <- data.table()

readcsv_binary_score <- function(csv){
    content <- read.csv(csv, header=FALSE)

    # strip first section's .txt., save parts
    idx <- regexpr(pattern="\t", content[1,1])
    author_doc <- substr(content[1,1], 1, idx[1] - 1)
    method <- substr(content[1,1],
                     idx[1] + 1,
                     nchar(as.character(content[1,1])))

    # Binary check
    # TODO! change from looking for "Correct" to match author in file names.
    #binary_result <- ifelse(content[2,2] == "Correct", 1, 0)
    # Find start of autho name
    idx_start_of_name <- tail(gregexpr(pattern="/", author_doc)[[1]], 1)
    idx_start_of_name <- ifelse(
        idx_start_of_name == -1,
        1,
        idx_start_of_name + 1
    )
    author_name <- substr(author_doc, idx_start_of_name, nchar(author_doc) - 4)

    # Find ending of author name
    idx_end <- gregexpr(pattern="_", author_name)[[1]][1]
    idx_end <- ifelse(
        idx_end == -1,
        nchar(author_name),
        idx_end - 1
    )
    author_name <- substr(author_name, 1, idx_end)

    binary_result <- ifelse(grepl(author_name, content[2,2]), 1, 0)

    if (length(binary_success) == 0){ #Empty data.table, fill it.
        binary_success <<- data.table(Questioned_Doc=author_doc,ID=1)
        binary_success[,method] <<- binary_result
    } else { # Non-Empty data.table
        # find row and col if author doc and method exist, respectively
        row_idx <- match(author_doc, binary_success$Questioned_Doc, nomatch=-1)
        col_idx <- match(method, colnames(binary_success), nomatch=-1)

        if (row_idx != -1 & col_idx != -1){ # Already exists
            if (is.na(binary_success[row_idx, col_idx, with=FALSE])){
                # First spot, is NA, fill it.
                binary_success[row_idx, col_idx] <- binary_result
            } else {
                # Find the first that contains a NA.
                rows <- which(binary_success$Questioned_Doc == author_doc)
                row_idx <- match(NA,
                                 unlist(binary_success[rows,
                                                       col_idx,
                                                       with=FALSE]),
                                 nomatch=-1)
                row_idx <- ifelse(row_idx == -1, -1, rows[row_idx])
                # NEED to find first NA, if any, and set row_idx to that!!!

                if (row_idx != -1){
                    binary_success[row_idx, col_idx] <- binary_result
                } else {
                    # If ALL existing rows do not have NAs, then add new row
                    # Like Insert new row, but with corresponding unique ID
                    tmp <- rep(NA, ncol(binary_success))
                    tmp[col_idx] <- binary_result
                    tmp[1] <- author_doc
                    id <- length(grep(author_doc,
                                    binary_success$Questioned_Doc,
                                    fixed=TRUE)) + 1
                    tmp[2] <- id # assign first ID
                    binary_success <<- rbind(binary_success, as.list(tmp))
                }
            }
        } else if(row_idx != -1 & col_idx == -1){# row exists, insert new col
            tmp <- rep(NA, nrow(binary_success))
            tmp[row_idx] <- binary_result
            binary_success[, method] <<- tmp
        } else if(row_idx == -1 & col_idx != -1){# col exists, insert new row
            tmp <- rep(NA, ncol(binary_success))
            tmp[col_idx] <- binary_result
            tmp[1] <- author_doc
            tmp[2] <- 1 # assign first ID
            binary_success <<- rbind(binary_success, as.list(tmp))
        } else { # Neither exist, insert new row and col.
            tmp <- rep(NA, nrow(binary_success))
            binary_success[, method] <<- tmp

            number_columns <- ncol(binary_success)
            tmp <- rep(NA, number_columns)
            tmp[number_columns] <- binary_result
            tmp[1] <- author_doc
            tmp[2] <- 1 # assign first ID
            binary_success <<- rbind(binary_success, as.list(tmp))
        }
        binary_success[,method] <<-
            apply(binary_success[,method,with=FALSE],
                  2,
                  function(x) as.numeric(x))
    }
    return(NULL)
}

path <- "../../results/Greek_Tri-CSV/"
sapply(paste(path, list.files(path, pattern="[.]csv$", recursive=TRUE), sep=""),
    readcsv_binary_score)

# Method count has been modified by above line, matrix made if no errors.
# Convert ID column to numeric
binary_success[,2] <- apply(binary_success[,2], 2, function(x) as.numeric(x))

# TODO need to ensure all binary tallies are numeric!
prob_success <- colMeans(binary_success[,3:ncol(binary_success)])

# P(m | n):
# cond <- which(n == 1)
# sum(m[cond] == 1) / length(cond)

# get permutations of all columns in pairs.
library(arrangements)

# Creates the conditional probability matrix from the provided binary success dt
# Not the most efficient method, but it works.
conditional_prob <- function(dat){
    cond_prob_mat <- matrix(nrow=ncol(dat),
                            ncol=ncol(dat),
                            dimnames=list(names(dat),names(dat)))

    col_perm <- permutations(k=2, x=1:ncol(dat), replace=TRUE)

    # decided a helper function is neater for readability
    construct_mat <- function(m, n, midx, nidx){
        if (midx == nidx){
            cond_prob_mat[midx, nidx] <<- 1
        } else {
            cond <- which(n == 1)
            cond_prob_mat[midx, nidx]<<- sum(m[cond] == 1) / length(cond)
        }
    }

    for (i in 1:nrow(col_perm)){
        construct_mat(dat[,col_perm[i,1], with=FALSE],
                      dat[,col_perm[i,2], with=FALSE],
                      col_perm[i,1],
                      col_perm[i,2])
    }

    return(cond_prob_mat)
}

cond_prob_mat <- conditional_prob(binary_success[,3:ncol(binary_success)])
