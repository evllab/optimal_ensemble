#!usr/bin/env Rscript
# Uses mRMRe package to gather the specified number of features.
# author: Derek S. Prijatelj

args = commandArgs(trailingOnly=TRUE)

library("mRMRe")
library("dplyr")
library("dtplyr")

# TODO get from
#path <- "../../results/greek-english-essays_enum.csv"
if (length(args) < 1){
    stop("A path to the existing enumerated csv, and a positive integer <= ",
        "number of features in provided csv must be provided as arguments to ",
        "the script when called.")
} else if (length(args) == 1){
    path = args[1]
    feature_count = 1
} else {
    path = args[1]
    feature_count = as.numeric(args[2])
}

# import binary successes
dat <- read.csv(path, header=TRUE, check.names=FALSE)
# bind target "True Author" to features
dat_bound <- cbind(dat[,1], dat[,3:ncol(dat)])

# Modify strings to ordered factors
#df <- mutate_each(dat, as.ordered) # change to ordered factors
df <- mutate_all(dat_bound, as.ordered) # change to ordered factors

# Convert to mRMR data
dd <- mRMR.data(data=df)

#run ensemble mRMR on data
fs <- mRMR.ensemble(
    data=dd,
    target_indices=1,
    feature_count=feature_count,
    solution_count=1
)

fs_solution <- solutions(fs)

#print(fs_solution)
#print(unlist(fs_solution))
#print(unlist(fs_solution) + 1)

#print(colnames(dat)[unlist(fs_solution) + 1])

# names of the features selected from mRMR:
print(colnames(dat_bound)[unlist(fs_solution)])
