#! /usr/local/bin/Rscript
#
#
# @author Derek S. Prijatelj

# TODO need to add CLI usage to complete the script!

library("data.table") # For speed and optimizations over data.frame

args = commandArgs(trailingOnly=TRUE)
if (length(args) < 2){
    stop("Must provide arguments: [path/to/root_src_dir] [path/to/output].n",
        call=False
    )
} else {
    src_path <- toString(args[1])
    dest_path <- toString(args[2])
}

#global var
enum_samples <- data.table()

get_author_name <- function(correct_author_doc){
    # Find start of author name
    idx_start_of_name <- tail(gregexpr(pattern="/", correct_author_doc)[[1]], 1)
    idx_start_of_name <- ifelse(
        idx_start_of_name == -1,
        1,
        idx_start_of_name + 1
    )
    correct_author_name <- substr(
        correct_author_doc,
        idx_start_of_name,
        nchar(as.character(correct_author_doc)) - 4
    )

    # Find ending of author name
    idx_end <- gregexpr(pattern="_", correct_author_name)[[1]][1]
    idx_end <- ifelse(
        idx_end == -1,
        nchar(correct_author_name),
        idx_end - 1
    )
    return(substr(correct_author_name, 1, idx_end))
}

readcsv_enum_table <- function(csv){
    content <- read.csv(csv, header=FALSE)

    # strip first section's .txt., save parts
    idx <- regexpr(pattern="\t", content[1,1])
    correct_author_doc <- substr(content[1,1], 1, idx[1] - 1)
    method <- substr(content[1,1],
                     idx[1] + 1,
                     nchar(as.character(content[1,1])))
    correct_author_name <- get_author_name(correct_author_doc)
    sample_result <- get_author_name(content[2,2])

    if (length(enum_samples) == 0){ #Empty data.table, fill it.
        enum_samples <<- data.table(
            Questioned_Doc=correct_author_doc,
            Questioned_Author=correct_author_name,
            ID=1
        )
        enum_samples[,method] <<- sample_result
    } else { # Non-Empty data.table
        # find row and col if author doc and method exist, respectively
        row_idx <- match(
            correct_author_doc,
            enum_samples$Questioned_Doc,
            nomatch=-1
        )
        col_idx <- match(method, colnames(enum_samples), nomatch=-1)

        if (row_idx != -1 & col_idx != -1){ # Already exists
            if (is.na(enum_samples[row_idx, col_idx, with=FALSE])){
                # First spot, is NA, fill it.
                enum_samples[row_idx, col_idx] <- sample_result
            } else {
                # Find the first that contains a NA.
                rows <- which(enum_samples$Questioned_Doc == correct_author_doc)
                row_idx <- match(NA,
                                 unlist(enum_samples[rows,
                                                     col_idx,
                                                     with=FALSE]),
                                 nomatch=-1
                )
                row_idx <- ifelse(row_idx == -1, -1, rows[row_idx])
                # NEED to find first NA, if any, and set row_idx to that!!!

                if (row_idx != -1){
                    enum_samples[row_idx, col_idx] <- sample_result
                } else {
                    # If ALL existing rows do not have NAs, then add new row
                    # Like Insert new row, but with corresponding unique ID
                    tmp <- rep(NA, ncol(enum_samples))
                    tmp[col_idx] <- sample_result
                    tmp[1] <- correct_author_doc
                    id <- length(grep(correct_author_doc,
                                    enum_samples$Questioned_Doc,
                                    fixed=TRUE)) + 1
                    tmp[2] <- correct_author_name
                    tmp[3] <- id # assign first ID
                    enum_samples <<- rbind(enum_samples, as.list(tmp))
                }
            }
        } else if(row_idx != -1 & col_idx == -1){# row exists, insert new col
            tmp <- rep(NA, nrow(enum_samples))
            tmp[row_idx] <- sample_result
            enum_samples[, method] <<- tmp
        } else if(row_idx == -1 & col_idx != -1){# col exists, insert new row
            tmp <- rep(NA, ncol(enum_samples))
            tmp[col_idx] <- sample_result
            tmp[1] <- correct_author_doc
            tmp[2] <- correct_author_name
            tmp[3] <- 1 # assign first ID
            enum_samples <<- rbind(enum_samples, as.list(tmp))
        } else { # Neither exist, insert new row and col.
            tmp <- rep(NA, nrow(enum_samples))
            enum_samples[, method] <<- tmp

            number_columns <- ncol(enum_samples)
            tmp <- rep(NA, number_columns)
            tmp[number_columns] <- sample_result
            tmp[1] <- correct_author_doc
            tmp[2] <- correct_author_name
            tmp[3] <- 1 # assign first ID
            enum_samples <<- rbind(enum_samples, as.list(tmp))
        }
        enum_samples[,method] <<-
            apply(enum_samples[,method,with=FALSE],
                  2,
                  function(x) as.character(x)
            )
    }
    return(NULL)
}

sapply(
    paste(
        src_path, list.files(src_path, pattern="[.]csv$", recursive=TRUE),
        sep=""
    ),
    readcsv_enum_table
)

# Method count has been modified by above line, matrix made if no errors.
# Convert ID column to numeric
enum_samples[,3] <- apply(enum_samples[,3], 2, function(x) as.numeric(x))

#prob_success <- colMeans(enum_samples[,3:ncol(enum_samples)])
write.csv(enum_samples, dest_path)
