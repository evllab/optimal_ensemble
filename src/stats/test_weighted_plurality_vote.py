"""
unit test for weighted plurality vote
"""

import pandas as pd
from weighting import plurality_vote

def test():
    features = pd.DataFrame(
        [
         ['a', 'b', 'a'],
         ['b', 'b', 'b'],
         ['a', 'b', 'b']
        ],
        columns=['f1','f2','f3']
    )

    target = pd.DataFrame(
        [
         ['a', 'b'],
         ['b', 'a'],
         ['a', 'b']
        ],
        columns=['True Author','False Author']
    )

    weights = [1.0,0.5,0.25]
    #outcome: a, b, a

    expected_output_unweighted = pd.DataFrame(
        [
         [1, 0, 1, 1],
         [1, 1, 0, 1]
        ],
        index=['a','b'],
        columns=['Hits', 'False Alarms', 'Misses', 'Correct Rejections']
    )

    expected_output_weighted = pd.DataFrame(
        [
         [2, 0, 0, 1],
         [1, 0, 0, 2]
        ],
        index=['a','b'],
        columns=['Hits', 'False Alarms', 'Misses', 'Correct Rejections']
    )

    # unweighted:
    unweighted_output = plurality_vote(features, target)

    # weighted:
    weighted_output = plurality_vote(features, target, weights)

    # check results:
    print('unweighted:\n',(unweighted_output == expected_output_unweighted).all())
    print('weighted:\n',(weighted_output == expected_output_weighted).all())

if __name__ =='__main__':
    test()
