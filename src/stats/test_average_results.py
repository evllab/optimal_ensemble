"""
Test to confirm that average_results is working.
"""

import pandas as pd
from auto_test_ensemble import average_results

def list_of_MultiIndex_DataFrames():
    """ Tests the averaging of a list of same MultiIndexed DataFrames """

    # ensemble
    #   (mrmr, best ind)
    #       (eval, test)
    #           pd.DataFrame index=class, columns=H,F,M,C

    classes = ['a','b']
    toc = ['Hits', 'False Alarms', 'Misses', 'Correct Rejections']

    # 6 classes, 2 {eval, test}, 2 {mrmr, best ind}, same ensemble (1)
    multi_index_array = [
        ['ensemble'] * len(classes) * 2 * 2,
        ['best ind'] * (len(classes) * 2) + ['mrmr'] * (len(classes) * 2),
        (['eval'] * len(classes) + ['test'] * len(classes)) * 2,
        classes * 2 * 2
    ]

    print('multi_index_array')
    [print(len(x), x) for x in multi_index_array]

    result_1 = pd.DataFrame(
        [
        [3, 0, 2, 5],
        [3, 1, 2, 4],
        [2, 4, 3, 1],
        [3, 4, 2, 1],
        [3, 2, 2, 3],
        [3, 2, 2, 3],
        [1, 1, 5, 4],
        [1, 2, 5, 3]
        ],
        columns=toc,
        index=multi_index_array
    )

    result_2 = pd.DataFrame(
        [
        [1, 1, 5, 4],
        [1, 2, 5, 3],
        [5, 3, 1, 2],
        [1, 5, 5, 0],
        [1, 1, 5, 4],
        [1, 1, 5, 4],
        [5, 3, 1, 2],
        [1, 5, 5, 0]
        ],
        columns=toc,
        index=multi_index_array
    )

    result_3 = pd.DataFrame(
        [
        [4, 0, 1, 5],
        [4, 1, 1, 4],
        [1, 4, 3, 1],
        [2, 4, 3, 1],
        [3, 4, 2, 1],
        [3, 4, 2, 1],
        [1, 4, 3, 1],
        [2, 4, 3, 1]
        ],
        columns=toc,
        index=multi_index_array
    )

    results = [result_1, result_2, result_3]
    print('results\n', results)

    #mean:
    def m(a,b,c):
        return sum([a,b,c])/3

    expected_output = pd.DataFrame(
        [
        [m(3,1,4), m(0,1,0), m(2,5,1), m(5,4,5)],
        [m(3,1,4), m(1,2,1), m(2,5,1), m(4,3,4)],
        [m(2,5,1), m(4,3,4), m(3,1,3), m(1,2,1)],
        [m(3,1,2), m(4,5,4), m(2,5,3), m(1,0,1)],
        [m(3,1,3), m(2,1,4), m(2,5,2), m(3,4,1)],
        [m(3,1,3), m(2,1,4), m(2,5,2), m(3,4,1)],
        [m(1,5,1), m(1,3,4), m(5,1,3), m(4,2,1)],
        [m(1,1,2), m(2,5,4), m(5,5,3), m(3,0,1)]
        ],
        columns=toc,
        index=multi_index_array
    )
    print('expected_output\n', expected_output)

    output = average_results(results)
    print('output\n', output)

    print('output == expected_output:\n', (output == expected_output).all())

if __name__ == '__main__':
    list_of_MultiIndex_DataFrames()
