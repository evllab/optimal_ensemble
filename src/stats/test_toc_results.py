"""
Test to ensure that toc_results works.
:author: Derek S. Prijatelj
"""

import pandas as pd
from weighting import toc_results

def test():
    # Create input and expected output
    pred_df = pd.DataFrame(
        [
        'a','c','d','a','a',
        'b','c','d','b','b',
        'c','b','c','e','f',
        'd','d','d','e','f',
        'e','e','c','d','e',
        'f','f','c','d','f',
        ],
        columns=['pred']
    )

    print('pred_df:\n', pred_df)

    test_data = pd.DataFrame(
        [
         ['a','b'],
         ['a','c'],
         ['a','d'],
         ['a','e'],
         ['a','f'],

         ['b','a'],
         ['b','c'],
         ['b','d'],
         ['b','e'],
         ['b','f'],

         ['c','a'],
         ['c','b'],
         ['c','d'],
         ['c','e'],
         ['c','f'],

         ['d','a'],
         ['d','b'],
         ['d','c'],
         ['d','e'],
         ['d','f'],

         ['e','a'],
         ['e','b'],
         ['e','c'],
         ['e','d'],
         ['e','f'],

         ['f','a'],
         ['f','b'],
         ['f','c'],
         ['f','d'],
         ['f','e'],
        ],
        columns=['True Author', 'False Author']
    )

    print('test_data:\n', test_data)

    expected_output = pd.DataFrame(
        [
         [3,0,2,5],
         [3,1,2,4],
         [2,4,3,1],
         [3,4,2,1],
         [3,2,2,3],
         [3,2,2,3]
        ],
        columns=['Hits', 'False Alarms', 'Misses', 'Correct Rejections'],
        index=['a','b','c','d','e','f']
    )

    print('expected_output:\n', expected_output)

    # Test
    pred_df_output = toc_results(pred_df, test_data)
    # Compare output to expected output
    print('pred_df_output == expected_output: \n',
        (pred_df_output == expected_output).all()
    )

    # Test pred input as dataframe and series
    pred_series_output = toc_results(pred_df.iloc[:,0], test_data)
    # Compare
    print('pred_series_output == expected_output:\n',
        (pred_series_output == expected_output).all()
    )

if __name__ == '__main__':
    test()
