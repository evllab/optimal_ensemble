TODO
==
1. Make the conditional prob matrix code a real script with CLI.
2. Take recursive collect of data from conditional prob, make it its own function
3. Use the gathered data to create enumerations of all the authors.
ONLY works when there is only one doc to be correctly classified to as.
Should work in our case. Either way, keep it in mind
4. create target vectors with the desired elements (correct author) in each sample.
5. Apply mRMRe package, see if it works, otherwise, we're writing it from scratch.

From this, mRMR will provide the minimum redundant and maximum relevant classifiers for authorship attribution.
With that information and the actual probability of successful classification for each classifier, a proper ensemble should be able to be constructed.

How we do that? I don't know right now...
