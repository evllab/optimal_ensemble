"""
Unit test to check if unweighted plurality vote works.
"""

import numpy as np
import pandas as pd
from weighting import plurality_unweighted, toc_results

def test():
    train_features = pd.DataFrame(
        [
         ['a', 'b', 'a'],
         ['b', 'b', 'b'],
         ['a', 'b', 'b']
        ],
        columns=['f1','f2','f3']
    )
    test_features = train_features.copy()

    train_target = pd.DataFrame(
        [
         ['a', 'b'],
         ['b', 'a'],
         ['a', 'b']
        ],
        columns=['True Author','False Author']
    )
    test_target = train_target.copy()

    expected_output_odd = pd.DataFrame(
        [
         [1, 0, 1, 1],
         [1, 1, 0, 1]
        ],
        index=['a','b'],
        columns=['Hits', 'False Alarms', 'Misses', 'Correct Rejections']
    )
    expected_output_odd = (expected_output_odd, expected_output_odd)

    np.random.seed(0)
    expected_output_even = pd.Series(
        [np.random.choice(['a','b']),
         'b',
         np.random.choice(['a','b'])]
    )
    results_even = toc_results(expected_output_even, train_target)
    expected_output_even = (results_even, results_even)

    output_odd = plurality_unweighted(train_features, train_target,
        test_features, test_target)

    np.random.seed(0)
    output_even = plurality_unweighted(
        train_features.iloc[:,0:2],
        train_target.iloc[:,0:2],
        test_features.iloc[:,0:2],
        test_target.iloc[:,0:2]
    )

    print('Odd: output == expected_output')
    print('eval:\n', (output_odd[0] == expected_output_odd[0]).all())
    print('test:\n', (output_odd[1] == expected_output_odd[1]).all())

    print('Even: output == expected_output')
    print('eval:\n', (output_even[0] == expected_output_even[0]).all())
    print('test:\n', (output_even[1] == expected_output_even[1]).all())

if __name__ == '__main__':
    test()
