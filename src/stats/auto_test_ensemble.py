"""
Python wrapper script for automating the creation and cross validation testing
of ensembles from the provided classifiers via path to root directory.

:author: Derek Prijatelj
"""

from datetime import datetime
from collections import Iterable
import os
import sys
import subprocess
import argparse
from multiprocessing import Pool
from multiprocessing.pool import ThreadPool
import numpy as np
import pandas as pd
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score
import weighting

def best_individual_feature_selection(train, feature_count):
    """ Output list of column names """
    accuracy = [accuracy_score(train['True Author'], train[x]) for x in
        train.iloc[:,2:]]

    sorted_acc, original_idx = zip(*sorted(zip(accuracy,range(len(accuracy)))))

    return list(
        train.iloc[:,np.asarray(original_idx[:feature_count]) + 2].columns)

def mRMR_feature_selection(train, feature_count):
    # TODO create temporary or pipe train data csv to Rscript!

    # create temporary csv to use for this
    tmp_name = '__temporary_train_mrmr_csv__' + str(datetime.now()) + '.csv'

    # check if file already exists, if so, append incremental number on it
    if os.path.exists(tmp_name):
        idx = tmp_name.rfind(';')
        value = 1 + int(tmp[idx + 1:-4]) if idx != -1 else 1

        if idx == -1:
            idx = -4
        tmp_name = tmp_name[:idx] + ';' + str(value) + '.csv'

    train.to_csv(tmp_name, index=False)

    print('mRMR train\n', train)

    # call Rscript with tmp csv
    output = subprocess.check_output([
        'Rscript',
        '--vanilla',
        'mRMRe.r',
        tmp_name,
        str(feature_count)
    ]).strip().decode()
    # NOTE, will return 'dat[,1]' or the target index if not enough samples nor features.
    # TODO Ensure that no more feaeture_count than features in data

    # delete temporary csv
    os.remove(tmp_name)

    return output.split('"')[1::2]

    #return output[output.index('"') + 1 : output.rfind('"')].split('" "')

def feature_selection(train, feature_count=2):
    # call mRMRe.r for ensemble size of 10 *always ordered so only take top N
    mRMR_train_features = mRMR_feature_selection(train, feature_count)

    # same for Individually Performing, take top N from 10
    best_individual_train_features = best_individual_feature_selection(train,
        feature_count)

    return mRMR_train_features, best_individual_train_features

def create_eval_ensemble(train, test, feature_count=2, exhaustive=False,
        ensembles=['plurality_unweighted']):
    """
    Create and evaluate all ensembles from this train and test data.

    :param train: classification results treated as training data
    :param test: classification results treated as testing data
    :param ensembles: tuple of ensemble models to eval and test.
    """

    print('train')
    print(train)
    print('test')
    print(test)

    #TODO add exhaustive feature selection! May add in caller of create_...
    #[feature_selection(train, i) for i in range(2, max(2, feature_count))]
    mRMR, best_individual = feature_selection(train, feature_count)

    print('mRMR len = ', len(mRMR))
    [print(x) for x in mRMR]
    print('best_individual len = ', len(best_individual))
    print(best_individual)

    # TODO remove list() conversion after testing done!
    train_test_args = list(zip(
        [train[mRMR], train[best_individual]],
        [train[['True Author', 'False Author']]] * 2,
        [test[mRMR], test[best_individual]],
        [test[['True Author', 'False Author']]] * 2
    ))

    ensemble_results = {}
    # Multiprocess the creation and evaluation of each ensemble on this data
    #with Pool(processes=len(ensembles) * 2) as pool:
    """
    with Pool(processes=1) as pool:
        if 'plurality_unweighted' in ensembles:
            # Non-Weighted Plurality
            #ensemble_results['plurality'] = pool.starmap_async(
            ensemble_results['plurality'] = pool.starmap(
                weighting.plurality_unweighted,
                train_test_args)#.get()

            print('plurality\n{}\n'.format(ensemble_results['plurality']))

        # Weighted Plurality
        if 'plurality_mi' in ensembles:
            #ensemble_results['plurality_mi'] = pool.starmap_async(
            ensemble_results['plurality_mi'] = pool.starmap(
                weighting.plurality_mutual_info,
                train_test_args)#.get()

        # SVMs
        if 'svm' in ensembles or 'svm' in ensembles:
            #ensemble_results['svm'] = pool.starmap_async(
            ensemble_results['svm'] = pool.starmap(
                weighting.svm,
                train_test_args)#.get()

        if 'perceptron' in ensembles:
            #ensemble_results['perceptron'] = pool.starmap_async(
            ensemble_results['perceptron'] = pool.starmap(
                weighting.perceptron,
                train_test_args)#.get()
    """

    print('ensembles: ', ensembles)
    print('args: ', args)
    print('train_test_args, again:')
    [print('{}\n'.format(x)) for x in train_test_args]

    for e in ensembles:
        ensemble_results[e] = []

    for x in list(train_test_args):
        print('\nx!\n')
        [print('{}\n'.format(y)) for y in x]
        if 'plurality_unweighted' in ensembles:
            # Non-Weighted Plurality
            ensemble_results['plurality_unweighted'].append(weighting.plurality_unweighted(*x))

            print('\nplurality done\n{}\n'.format(ensemble_results['plurality_unweighted']))

        # Weighted Plurality
        if 'plurality_mi' in ensembles:
            ensemble_results['plurality_mi'].append(weighting.plurality_mutual_info(*x))
            print('\nplurality MI done\n{}\n'.format(ensemble_results['plurality_mi']))

        # SVMs
        if 'svm' in ensembles or 'svm' in ensembles:
            ensemble_results['svm'].append(weighting.svm(*x))

    print('Ensemble_Results\n',ensemble_results)

    # Concatentate and name each part of the result dataframe
    results =[]
    for ensemble, result in ensemble_results.items():
        mrmr = pd.concat([result[0][0], result[0][1]], keys=['eval', 'test'])
        best = pd.concat([result[1][0], result[1][1]], keys=['eval', 'test'])

        ensemble_result = pd.concat([mrmr, best],
            keys=['mRMR', 'Best Individual'])

        results.append(pd.concat([ensemble_result], keys=[ensemble]))

    # TODO ensure the columns are Hits, False Alarms, Misses, Correct Rejections
    print('results', results)
    return  pd.concat(results)

def average_results(results):
    """
    Given iterable of data frames of same shape, finds means for each row,
    and column pair.

    :param results: list of tuples of eval and test result data frames.
    """
    # Shallow type checking
    if isinstance(results, list) and isinstance(results[0], tuple) \
            and isinstance(results[0][0], pd.DataFrame):
        # Separate the eval and test results, then perform averaging.
        # as of now, the results = [(eval1, test1), (eval2, test2), ...]
        eval_results, test_results= zip(*results)

        df_concat = pd.concat(eval_results)
        averaged_eval_results = df_concat.group_by(df_concat.index).mean()

        df_concat = pd.concat(test_results)
        averaged_test_results = df_concat.group_by(df_concat.index).mean()

        return averaged_eval_results, averaged_test_results
    elif isinstance(results, list) \
            and isinstance(results[0], pd.DataFrame):
        df_concat = pd.concat(results)
        df_concat = df_concat.groupby(df_concat.index).mean()
        return df_concat.reindex(pd.MultiIndex.from_tuples(df_concat.index))
    else:
        raise TypeError("results must be a list of either tuple paired data "
            + "frames or a list of data frames.")

def get_author_rows(author_dict, authors):
    """
    Given authors to set of row indices, and authors, return list of all
    rows that contain an author in the authors. Author sets contain all row
    indices pertaining to all authors. To find the set of rows that pertains only to the set of specified authors,
    these various sets must be compared.
    """
    rows = set()

    if authors.size < 2:
        raise('Cannot have less than two authors in either train or test sets. '
            + 'They would be evaluating against themselves. '
            + 'Check K-folds and number of authors.')

    for author in authors:
        others = set()
        [others.update(others | author_dict[other]) for other in authors
            if other != author]
        rows.update(author_dict[author] & others)

    return list(rows)

def kfold_cv(enum_data, authors, k=10, feature_count=2, exhaustive=False,
        ensembles=['plurality_unweighted']):
    """
    K-Fold Cross Validation

    :param enum_data: file path to root dir of all data files
    :param author_dir: file path to dir of author_files.csvs
    """
    # make different train and test sets from enum_data
    # Shuffle the AUTHORS
    #authors_keys = list(authors.keys())
    authors_keys = np.asarray(list(authors.keys()))
    kf = KFold(k, shuffle=True)
    author_pairs = [(authors_keys[train], authors_keys[test]) for train, test in
        kf.split(authors_keys)]

    print('author_pairs')
    [print('{}\n'.format(x)) for x in author_pairs]

    kargs = [(enum_data.iloc[get_author_rows(authors, train)],
        enum_data.iloc[get_author_rows(authors, test)],
        feature_count,
        exhaustive,
        ensembles
        ) for train, test in
        author_pairs]

    print('kargs to create_eval_ensemble')
    [print('{}\n'.format(x)) for x in kargs]

    # TODO add ability to select different ensembles. esp. from CLI

    # Distribute & compute the different CV folds
    #with ThreadPool(processes=k) as pool:
    #with ThreadPool(processes=1) as pool:
        #results = pool.starmap_async(create_eval_ensemble, kargs)#.get()
        #results = pool.starmap(create_eval_ensemble, kargs)#.get()

    results = []
    for karg in kargs:
        results.append(create_eval_ensemble(*karg))

    return average_results(results)

def get_authors(authors):
    """
    Takes the pandas dataframe columns and finds all unique values and maps them
    to their original row indices.

    :param authors: column of authors invovled in each sample
    :returns: dict('author':set(row_numbers))
    """
    # get all unique authors and map them to their orginal index
    # NOTE assumes that only 1 occurrence of each author, uses set()
    author1 = np.unique(authors['True Author'], return_inverse=True)
    author1 = {author:set(np.where(author1[1]==i)[0]) for i, author in
        enumerate(author1[0])}

    author2 = np.unique(authors['False Author'], return_inverse=True)
    author2 = {author:set(np.where(author2[1]==i)[0]) for i, author in
        enumerate(author2[0])}

    # Save original indices where author is in both author1 and author2
    author_dict = {x:author1[x]|author2[x] for x in
        author1.keys() & author2.keys()}

    # Add keys missing from author1
    author_dict.update(
        {x:author1[x] for x in author1.keys() - author_dict.keys()})

    # Add keys missing from author1
    author_dict.update(
        {x:author2[x] for x in author2.keys() - author_dict.keys()})

    return author_dict

def nested_kfold_cv(enum_data, n=50, k=10, feature_count=2,
        exhaustive=False, ensembles=['plurality_unweighted']):
    """
    Nested K-Fold Cross Validation
    :returns: DataFrame with index as a MultiIndex to indicate eval, test sets,
        ensembles, and their respective classes and columns = TOC base variables
    """
    authors = get_authors(enum_data[['True Author','False Author']])

    #kfold_args = zip([enum_data] * n, [authors] * n, [k] * n)
    # repeats the CV multiple times to overcome biased selection via iterations
    #with Pool(processes=n) as pool:
    #    results = pool.starmap_async(kfold_cv, kfold_args).get()

    args = (enum_data, authors, k, feature_count, exhaustive, ensembles)
    results = [kfold_cv(*args) for i in range(n)]

    # TOC: H, M, F, C
    return average_results(results)

def auto_ensemble_test(enum_data_filepath, output_path=None, n=50, k=10,
        feature_count=2, exhaustive=False,
        ensembles=['plurality_unweighted'],
        identifier=''):
    # load enum_data_filepath
    enum_data = pd.read_csv(enum_data_filepath)

    # Save results
    if output_path is None or os.sep in output_path:
        identifier = identifier.replace('..' + os.sep,'--;')
        identifier = identifier.replace(os.sep,';').replace('..','--')
        identifier = identifier.replace(' ','').replace('[','').replace(']','')
        identifier = identifier.replace('\'','').replace('"','')
        output_file = identifier + '_toc_results_' + str(datetime.now()) + '.csv.gz'

        if output_path is not None:
            output_path = os.path.join(output_path, output_file)
        else:
            output_path = output_file

    # make the dir first and ensure it exists before doing the work.
    if not os.path.exists(output_path):
        if os.sep in output_path and os.path.dirname(output_path) != '':
            os.makedirs(os.path.dirname(output_path), exist_ok=True)

    assert(pd.Series(['True Author', 'False Author']).isin(
        enum_data.columns).all())
    results = nested_kfold_cv(enum_data, n, k, feature_count, exhaustive,
        ensembles)

    results.to_csv(output_path, compression='gzip')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Perform nested k-fold cross validated ensemble creation '
        + 'and evaluation on the provided csv of True, False authors, and '
        + 'features.'
    )

    parser.add_argument('enumerated_data_filepath',
        type=str,
        help='Enter the file path to the enumerated data csv'
    )

    parser.add_argument('-n', '--nested_iterations',
        default=50,
        type=int,
        help='Number of nested iterations of k-fold cross validation',
        metavar='nested iterations'
    )

    parser.add_argument('-k', '--k_folds',
        default=10,
        type=int,
        help='Number (k) of k-folds for k-fold cross validation',
        metavar='k folds'
    )

    parser.add_argument('-o', '--output_filepath',
        default=None,
        type=str,
        help='Enter the file path to the enumerated data csv',
        metavar='output filepath'
    )

    parser.add_argument('-f', '--feature_count',
        default=2,
        type=int,
        help='Enter the number of features in the resulting ensemble.',
        metavar='feature count'
    )

    # TODO is type=bool necessary?
    parser.add_argument('-F', '--feature_count_exhaustive_testing',
        default=False,
        action='store_const',
        const=True,
        help='Test all feature count sizes below the specified feaeture count with the minimum being 2.',
        metavar='feature count exhaustive testing'
    )

    parser.add_argument('-e', '--ensembles',
        default=['plurality_unweighted'],
        type=str,
        nargs='+',
        help='Enter the types of ensemble voting to test from: '
            + 'plurality_unqeighted,'
            + 'plurality_mi, '
            + 'svm',
        metavar='ensembles'
    )

    args = parser.parse_args()

    auto_ensemble_test(
        args.enumerated_data_filepath,
        args.output_filepath,
        args.nested_iterations,
        args.k_folds,
        args.feature_count,
        args.feature_count_exhaustive_testing,
        args.ensembles,
        'n' + str(args.nested_iterations) \
            + 'k' + str(args.k_folds) \
            + 'f' + str(args.feature_count)
            + '_e' + str(args.ensembles)
    )
