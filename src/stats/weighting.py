"""
Multiple different weighting functions and their associated prediction function.
:author: Derek S. Prijatelj
"""

from collections import Counter
from random import getrandbits
import numpy as np
import pandas as pd #perhaps unnecessary
from sklearn import svm
from sklearn.preprocessing import StandardScaler, OneHotEncoder, LabelEncoder
from sklearn.feature_selection import mutual_info_classif
from sklearn.svm import SVC
#from hpsklearn import HyperoptEstimator, svc_rbf

# TODO for memory reasons, make the measure_* not store any variables.
def measure_hits(pred, test):
    """
    Author A IDed as Author A: True = A, Pred = A, Focus = True/Pred

    NOTE author in True is the class
    """
    hits = pd.DataFrame(
        test['True Author'] == pred,
        dtype='int32'
    ).rename(test['True Author'])

    return hits.groupby(hits.index).sum()

def measure_false_alarms(pred, test):
    """
    Not Author A IDed as Author A: True = Not A, Pred = A, Focus = Pred

    NOTE pred is class, so NaNs may occur if a class is not in pred, set NaN = 0
    """
    false_alarms = pd.DataFrame(
        test['True Author'] != pred,
        dtype='int32'
    ).rename(pred)

    return false_alarms.groupby(false_alarms.index).sum()

def measure_misses(pred, test):
    """
    Author A IDed as not Author A: True = A, Pred = Not A, Focus = True

    Note the author in True is the class
    """
    misses = pd.DataFrame(
        test['True Author'] != pred,
        dtype='int32'
    ).rename(test['True Author'])

    return misses.groupby(misses.index).sum()

def measure_correct_rejections(pred, test):
    """
    Not Author A IDed successfully when compared to Author A:
    True = Not A, Pred = Not A && True == Pred, Focus = False

    NOTE the author in False is the class
    """
    correct_rejections = pd.DataFrame(
        (test['True Author'] == pred)
        & (test['True Author'] != test['False Author'])
        & (pred != test['False Author']),
        dtype='int32'
    ).rename(test['False Author'])

    return correct_rejections.groupby(correct_rejections.index).sum()

def toc_results(pred, test_data, classifier_name=None):
    """
    Computes the TOC variables H, F, M, C for all classes for the ensemble.
    Assumes elements in target to be unique.

    :param pred: dataframe of predictions being measured
    :param test_data: dataframe of True Author, False Author columns
    """
    if isinstance(pred, pd.DataFrame):
        pred = pred.iloc[:,0]

    u = np.unique(test_data.values)
    print('unique values of test_data:\n', u)
    toc = pd.DataFrame(index=u)
    print('empty toc df :', toc)

    toc['Hits'] = measure_hits(pred, test_data)
    toc['False Alarms'] = measure_false_alarms(pred, test_data)
    # False Alarms uses pred as class, chance for NaNs if class not in pred
    toc['False Alarms'].fillna(0, inplace=True)
    toc['Misses'] = measure_misses(pred, test_data)
    toc['Correct Rejections'] = measure_correct_rejections(pred, test_data)

    # For each class, look at all other samples.
    #toc['Misses'] = [(pred.drop(pred.index[[i]]) == focus_author).sum()
    #    for i, focus_author in enumerate(test_data['True Author'])]

    print('\nTOC:')
    print('test_data:\n', test_data, '\n', test_data.index[[0]])
    print('pred:\n', pred, '\n', pred.index[[0]])
    print('\ntoc results:\n', toc)

    # toc results dataframe = classes = rows, HFMC = columns
    return toc

def plurality_vote(features, test_data, weights=None):
    """
    Takes the weighted plurality vote. First tallies the votes, then applies the
    weights.

    :param features: Data frame of features' votes, columns features, rows are
        samples
    :param test_data: Data frame of test labels: True Author, False Author
    :param weights: array-like structure that provides the weights to each
        feature
    """
    #pred = features.apply(
    #    lambda x: (x.value_counts() * weights).idxmax(),
    #    axis=1
    #)

    def weighted_vote(features):
        print('features in weighted_vote:\n', features)
        label_encoder = LabelEncoder()
        features_enc = label_encoder.fit_transform(features)
        print('features_enc:\n', features_enc)
        print('weights:\n', weights)

        count = np.bincount(features_enc, weights)
        print('count: ', count)
        pred = np.where(count == count.max())[0]
        print('pred from np.where: ', pred)

        pred = np.random.choice(pred) if pred.size > 1 else pred[0]

        print('pred after size check = ', pred)

        print('inv_transform_pred: ', label_encoder.inverse_transform(pred))

        return label_encoder.inverse_transform(pred)

    if pd.__version__ == '0.22.0':
        pred = features.apply(weighted_vote, axis=1, reduce=True)
    else:
        pred = features.apply(weighted_vote, axis=1, result_type='reduce')

    print('\nplurality_vote : pred :\n', pred)
    print('\nActal: :\n', test_data['True Author'])

    return toc_results(pred, test_data)

def plurality_unweighted(train_features, train_target, test_features,
        test_target):

    print('\nplurality unweighted')

    eval_results = plurality_vote(train_features, train_target)
    test_results = plurality_vote(test_features, test_target)

    return eval_results, test_results

def plurality_mutual_info(train_features, train_target, test_features,
        test_target):
    """

    """
    label_encoder = LabelEncoder()
    target_enc = label_encoder.fit_transform(train_target['True Author'])
    features_enc = train_features.apply(label_encoder.transform)

    print('target_enc: len = ', len(target_enc) ,'\n', target_enc)
    print('features_enc\n', features_enc)

    mi = mutual_info_classif(
            features_enc,
            target_enc.reshape(-1,1),
            True
    )

    #print('Features\n', train_features)
    #print('Actal: :\n', train_target['True Author'])
    print('Mutual Information (MI) = \n', mi)

    eval_results = plurality_vote(train_features, train_target, mi)
    test_results = plurality_vote(test_features, test_target, mi)

    return eval_results, test_results

# NOTE the svm and perceptron are buggy and not implemented yet!

def svm(train_features, train_target, test_features, test_target):
    """ trains and tests Support Vector Classifier """
    eval_estim = SVC()

    # Binary encode based on True and False. If True 1, else 0.
    binary_target = np.append(
        (np.ones((len(train_taget.index),1)),
            np.zeros((len(train_arget.index),1))),
        axis=0
    )
    tr_binary_features = train_features.apply(
        lambda x: x == train_target['True Author']
    )
    tr_binary_features = tr_binary_features.append(-tr_binary_features).astype(int)

    def rand_binary_mask(row):
        mask = random.getrandbits(1)

        return

    test_binary = pd.concat([test_target, test_features], axis=1)
    test_binary.apply(rand_binary_mask, inplace=True)

    # Train SVC
    eval_estim.fit(binary_features, binary_target)

    # Obtain predictions
    eval_pred = eval_estim.predict(tr_features_enc)
    test_pred = eval_estim.predict(
        one_hot_encoder.transform(
            test_features.apply(label_encoder.transform)
        )
    )

    #inverse the binary encoding:
    eval_pred = label_encoder.inverse_transform(eval_pred)
    ev
    test_pred = label_encoder.inverse_transform(test_pred)

    return (toc_results(pd.DataFrame(eval_pred), train_target),
        toc_results(pd.DataFrame(test_pred), test_target))

# TODO worry about implementing this after all others are completed
"""
def perceptron(train_features, tain_target, test_features, test_target):
    # import keras here for multiprocessing to work, TODO apparently
    from keras.models import Sequential
    from keras.layers import Dense, Activation

    # create perceptron network
    perceptron_model = Sequential([
        Dense(len(train_features.columns)),
        Activation('elu') # TODO hyperparameter learn this????
    ])
    perceptron_model.compile(
        optimizer='adam',
        loss='categorical_crossentropy',
        metrics=['accuracy']
    )

    # Encode the features and targets
    label_encoder = LabelEncoder()
    tr_target_enc = label_encoder.fit_transform(train_target['True Author'])
    tr_features_enc = train_features.apply(label_encoder.transform)

    one_hot_encoder = OneHotEncoder()
    tr_target_enc = one_hot_encoder.fit_transform(tr_target_enc)
    tr_features_enc = one_hot_encoder.transform(tr_features_enc)

    # Train the Perceptron
    # TODO Hyperparameter Optimization on epochs, etc.?????
    perceptron_model.fit(tr_features_enc, tr_target_enc, epochs=10, batch_size=32)
    # Obtain predictions
    eval_pred = perceptron_model.predict(tr_features_enc)
    test_pred = perceptron_model.predict(
        one_hot_encoder.transform(
            label_encoder.transform(test_features)
        )
    )

    # inverse one hot and label encodings of predictions
    eval_pred = label_encoder.inverse_transform(
        [np.where(x==1) for x in eval_pred])
    test_pred = label_encoder.inverse_transform(
        [np.where(x==1) for x in test_pred])

    return (toc_results(pd.DataFrame(eval_pred), train_target),
        toc_results(pd.DataFrame(test_pred), test_target))
"""
