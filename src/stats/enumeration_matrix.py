"""
Python script that recursively gathers all .csvs under a root directory and
saves the test data for binary classificaiton in one csv:
True Author, Author Option 1, Author Option 2, and the predicted author.

By doing depth first search, assumes that all unique lingusitic analysis methods
are contained in same branch in directory tree. This avoids having multiple NAs
at a time.
"""

from datetime import datetime
import os
import sys
import numpy as np
import pandas as pd

# TODO save result as the recursive process progresses to be memory efficient!

def author_pair_exists(dataframe, true_author, false_author):
    return ((dataframe['True Author'] == true_author)
            & (dataframe['False Author'] == false_author)).any()

def duplicate_check(dataframe, true_author, false_author, feature):
    if feature in dataframe.columns:
        return ((dataframe['True Author'] == true_author)
                & (dataframe['False Author'] == false_author)
                & (dataframe[feature].notna())).any()
    return False

def get_author(file_path):
    base = os.path.basename(file_path)
    return base[:base.index('_')]

def add_entry(csv_file, dataframe):
    """ Adds test data to master csv from provided csv file """
    csv_df = pd.read_csv(csv_file)

    # check if csv is valid, ie. formatted as expected, ow. skip
    if (csv_df.shape != (2, 3)
        #    or not np.all(csv_df.dtypes == [np.int, np.str, np.float])
            or 'File' != csv_df.columns[1]
            or 'Value' != csv_df.columns[2]):
        print('csv_df.dtypes = ', csv_df.dtypes)
        print('Warning: "' + csv_file + '" is not formatted correctly!\n'
            + str(csv_df.shape != (2, 3)) + '\n'
        #    + str(not np.all(csv_df.dtypes == [np.int, np.str, np.float])) +'\n'
            + str('File' != csv_df.columns[1]) +' "'+ csv_df.columns[1] + '"\n'
            + str('Value' != csv_df.columns[2]) +' "'+ csv_df.columns[2] + '"\n'
            + 'Skipping csv.\n'
        )
        return dataframe

    true_author = get_author(csv_df.columns[0])

    predicted_author = get_author(csv_df.iloc[0,1])
    a2 = get_author(csv_df.iloc[1,1])
    false_author = predicted_author if predicted_author != true_author else a2

    feature = ';'.join(csv_df.columns[0].split('\t')[1:])

    # duplicate check
    if (duplicate_check(dataframe, true_author, false_author, feature)):
        raise NameError('Authors and feature triple already exists in data '
            + 'set! Duplicates not supported:\n'
            + 'True = ' + true_author + '\n'
            + 'False = ' + false_author + '\n'
            + 'feature = ' + feature + '\n'
            + 'csv_file = ' + csv_file
        )
    else:
        # Check if to modify existing row, or add new
        if author_pair_exists(dataframe, true_author, false_author):
            # TODO may encounter multiple NAs, ensure single assignment only!
            if feature not in dataframe.columns:
                dataframe.loc[
                    ((dataframe['True Author'] == true_author)
                    & (dataframe['False Author'] == false_author)),
                    #& (dataframe[feature].isna())),
                    feature] = predicted_author
            else:
                if 1 < ((dataframe['True Author'] == true_author)
                    & (dataframe['False Author'] == false_author)
                    & (dataframe[feature].isna())).sum():
                        raise NameError('More than one element has been '
                        + 'assigned!')
                dataframe.loc[
                    ((dataframe['True Author'] == true_author)
                    & (dataframe['False Author'] == false_author)
                    & (dataframe[feature].isna())),
                    feature] = predicted_author
        else:
            dataframe = dataframe.append(pd.DataFrame(
                [[true_author, false_author, predicted_author]],
                columns=('True Author', 'False Author', feature)
            ))

    return dataframe

def recursive_gather(current_dir, dataframe):
    """
    Recursively traverses the root directory depth first and adds data to a
    master dataframe.
    """
    dir_list = os.listdir(current_dir)
    csvs = [os.path.join(current_dir, f) for f in dir_list
        if os.path.isfile(os.path.join(current_dir, f)) and '.csv' == f[-4:]]

    for c in csvs:
        dataframe = add_entry(c, dataframe)

    #leaf_dir = True
    for d in dir_list:
        if os.path.isdir(os.path.join(current_dir, d)):
            dataframe = recursive_gather(os.path.join(current_dir, d),
                dataframe)
            #leaf_dir = False

    # NOTE assumes that all similar analysis methods are in same dir, and not
    # separated any further. Then
    #if leaf_dir:
    #   dataframe.dropna('columns', inplace=True)
    return dataframe

def create_enumerated_csv(root_dir, output_path):
    # TODO ensure all other files follow this scheme:
    dataframe = pd.DataFrame(columns=('True Author', 'False Author'),
        dtype=np.str)

    if not os.path.exists(output_path):
        if os.sep not in output_path and os.path.dirname(output_path) != '':
            os.makedirs(os.path.dirname(output_path), exist_ok=True)
        elif os.sep in output_path:
            os.makedirs(output_path, exist_ok=True)

    assert(os.path.isdir(root_dir))
    dataframe = recursive_gather(root_dir, dataframe)

    # ensure proper ordering of first two columns:
    columns = dataframe.columns.tolist()
    columns.sort()
    true_author_idx = columns.index('True Author')
    false_author_idx = columns.index('False Author')

    columns = [columns[true_author_idx]] + [columns[false_author_idx]] \
        + [col for i, col in enumerate(columns)
            if i not in {true_author_idx, false_author_idx}]

    dataframe = dataframe[columns]

    dataframe.sort_values(['True Author', 'False Author'], inplace=True)

    if os.path.isdir(output_path):
        output_path = os.path.join(
            output_path,
            str(datetime.now()) + '.csv.gz'
        )

    # TODO check if multiple columns with NA's at a time!!!!
    # remove all NaN rows.
    dropped_df = dataframe.dropna('columns')

    if not dropped_df.equals(dataframe):
        print('WARNING: NAs were found and dropped from the data frame. '
            + 'Saved original with NAs by appending "including_NAs" to '
            + 'filename,')

        dataframe.to_csv(
            output_path + '.including_NAs',
            encoding='utf-8',
            index=False,
            compression='gzip'
        )

    dropped_df.to_csv(
        output_path,
        encoding='utf-8',
        index=False,
        compression='gzip'
    )

def main(argv):
    assert(len(argv) == 3)
    # pass root dir, and desired output dir
    # TODO ensure those file paths are exist/valid
    create_enumerated_csv(argv[1], argv[2])

if __name__ == '__main__':
    main(sys.argv)
