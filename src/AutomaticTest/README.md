﻿AutomaticTest program guide:
Run the AutomaticTest.java file in order to start the program.

For starters, you will need an Experiment.csv.  In order to create all possible combinations of the specific tests you want, create columns with these headers (If you don’t want one, for example you don’t want any event cullers, then you can ignore the header and not add it):  Names, Canonicizers, Event Drivers, Event Cullers, Analyses, Distance, and Corpus.  

Names is currently ignored, so there is no point in creating the column as of this moment.  However, in case you want to use a CSV you already had, the column does not cause any harm.

Canonicizers, Event Drivers, and Event Cullers should be listed as normal, pertaining to what you want.  All possible combinations will be created (Example: if you listed Canonicizer A and Canonicizer B, then the final creation will contain Canonicizer A, Canonicizer B, and Canonicizer A&B).

For Analyses and Distance, you will have to list whatever Analysis you want and attach any distance operation to it in the next column.  If you want a single Analysis but multiple Distance functions, you will have to list that analysis again in the analysis column, and list the next distance operator to it in the same row.

EX:  Say you want to run centroid driver by itself, and centroid driver with cosine distance.  You will need to have “Centroid Driver” in one row, followed by a blank in the distance column of the same row, and after that have “Centroid driver” followed by “Cosine Distance” in the next row, where Centroid Driver is in the analyses column and Cosine Distance is in the Distance column.

Don’t worry about any blank spaces in the columns/rows.

The final column, Corpus, takes a directory to a file full of txt documents, separated with (topic1_en and topic2_en) and/or (topic1_gr and topic2_gr).  You will need to have at both a topic1 and a topic2 for every file or else the corpus creation will fail.

CreateBiTest program guide:
Just input the path to the directory where your corpora is located.  Check the last paragraph of the previous section to make sure everything is set up correctly.
