//Author: Nicholas Marshman
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import java.lang.Process;

public class CreateBiTest{
	private static void runExp(String[][] arr, String pathJGAAP) throws Exception{
	
		String csv = "./EnvelopeAnalysis.csv";
		File file = new File("./Experiments");
		File[] fileList = file.listFiles();

		double count = 0;
		double current = 0;
		System.out.println("Starting JGAAP Analyses on the tests.");
		for(File f: fileList){	
			String str = f.toString();
			StringBuilder sb = new StringBuilder();
			PrintWriter pw = new PrintWriter(new File(csv));
			sb.append("EnvelopeAnalysis\n");
			for(int i = 0; i < 4; i++){
				String name = arr[0][i].replaceAll(","," "+(int)count+",");
				sb.append(name+arr[1][i]+arr[2][i]+arr[3][i]
					+arr[4][i]+arr[5][i]+ str +"\n");
			}
			pw.write(sb.toString());
			pw.close();
			
			String cmd = "java -jar "+pathJGAAP +" -ee "+csv;
			
			double percent = (count/(double)fileList.length*100);
			
			//TODO: Progress Bar Instead.
			if(((current+0.01)<=percent)||count==0){
				current = percent;
				System.out.print((int)count+"/"+fileList.length+ " | ");
				System.out.printf("%2.2f%% complete\n", percent);
			}
			Process p = Runtime.getRuntime().exec(cmd);
			p.waitFor();
			if(p.exitValue()!=0)
				System.out.println("Error occured within JGAAP");
			count++;
		}
		System.out.println("Done!");
	}
	public static void main(String args[]) throws Exception{
	
		//TODO: Set up ini for preferences on where the files are instead
			//of just reading from the keyboard.
			
		System.out.print("Enter the path to the corpus directory: ");
		Scanner sc = new Scanner(System.in);
		String corPath = sc.nextLine();
		//String corPath = "../../../Txt";
		System.out.print("Enter the CSV you want to use: ");
		String csvPath = sc.nextLine(); 
		//String csvPath = "../../../EnvelopeAnalysis.csv";
		System.out.print("Enter the path to JGAAP: ");
		String jgPath = sc.nextLine();
		//String jgPath = "../../../JGAAP.jar";
		sc.close();
		AutomaticTest at = new AutomaticTest();
		at.createCorpora(corPath);
		String arr[][] = at.readCSV(csvPath,true);
		for(String[] x: arr){
			for(int i = 0; i<x.length;i++){
				if(x[i]==null)
					x[i]="";
				else
					x[i] = x[i]+",";
			}
		}
		runExp(arr,jgPath);
	}
}
