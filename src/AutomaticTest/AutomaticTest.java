//Author: Nicholas Marshman
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import java.lang.Process;

//TODO: Get rid of the allStr ArrayList and replace it with a stack.
//TODO: Set up -g and -e extensions to run tests on gr/en respectively.
//TODO: Optimize and fix code to make it more readable.

public class AutomaticTest{
	private static ArrayList<String> nam = new ArrayList<String>();
	private static ArrayList<String> can = new ArrayList<String>();
	private static ArrayList<String> dri = new ArrayList<String>();
	private static ArrayList<String> cul = new ArrayList<String>();
	private static ArrayList<String> ana = new ArrayList<String>();
	private static ArrayList<String> dis = new ArrayList<String>();
	private static ArrayList<String> cor = new ArrayList<String>();
	private static String[] corpus;
	
	private static int nameCount = 0;
	private static int corCount = 0;
	private static int sbCount = 0;
	private static int sbTCount = 0;
	
	private static StringBuilder sb;
	
	private static void printWrite(StringBuilder sb,boolean cFlag)
					throws IOException{
		String path = "./RunExps/RunExperiment"+(++sbTCount)+".csv";
		PrintWriter pw = new PrintWriter(new File(path));
		pw.write(sb.toString());

		if(cFlag)
			pw.close();
	}
	/**
	* Prints all the possible combinations of tests to a CSV.
	* @param ArrayList<String> allStr - arraylist containing all the 
	* 	possible combinations.
	* @param StringBuilder sb - Stringbuilder that will be written to the CSV 
	*	file
	*/
	private static void printAll(ArrayList<String> allStr, 
					StringBuilder sb) throws IOException{
		String finalStr = "Test C".concat((corCount+1)+"N"+(nameCount+1)+",");
		nameCount++;
		allStr.add(corpus[corCount]);
		for(String str: allStr){
			if(checkStr(str).compareTo("")==0)
				continue;
			else
				finalStr = finalStr.concat(str + ",");
		}
		allStr.remove(allStr.size()-1);
		int x = finalStr.lastIndexOf(",");
		finalStr = finalStr.substring(0,x);
		if(sbCount < 3000000){
			sb.append(finalStr.concat("\n"));
			sbCount++;
		}
		else{
			printWrite(sb,false);		
			sb = new StringBuilder();
			sb.append("RunExperiment\n");
			sb.append(finalStr.concat("\n"));
		}
	}
	/**
	* Checks the event driver array to see if there are any or not, 
	* and then calls the checkCul function to check for associated distance
	* methods.
	*
	* @param String[][] allArr - 2d array that contains all associated arrays
	* @param boolean[] flags - boolean array full of the associated flags
	* @param ArrayList<String> allStr - an arrayList containing all the 
	* 	possible combinations of strings
	* @param int[] lengths - an array full of the associated array lengths
	* @param BufferedReader ec - a BR for the cul file, not used here but passed 	 
	*	to the checkCul function to be used.
	*/
	private static void checkDri(String[][] allArr, 
					boolean[] flags, ArrayList<String> allStr, 
					int[] lengths, BufferedReader ec,
					StringBuilder sb) throws IOException {

		boolean driFlag = flags[0];
		int driLength = lengths[0];
		if(driFlag != false){
			for(int i = 0; i < driLength; i++){
				if(driLength !=0) allStr.add(allArr[0][i]);
				checkCul(allArr,flags,allStr,lengths,ec,sb);
				allStr.remove(allStr.size()-1);
			}
		}else{ 
			checkCul(allArr,flags,allStr,lengths,ec,sb);
		}
		
	}
	/**
	* Checks the cul file for all possible combinations of event cullers
	* after which it will create all possible combinations of cullers and the
	* analysis + distance sections.
	*
	* It doesn't use most of the params other than allStr and ec, 
	* but it passes them on to the other functions.
	* @param String[][] allArr - 2d array that contains all associated arrays
	* @param boolean[] flags - boolean array full of the associated flags
	* @param ArrayList<String> allStr - an arrayList containing all the 
	* 	possible combinations of strings
	* @param int[] lengths - an array full of the associated array lengths
	* @param BufferedReader ec - the BR of the file that is being read.
	* @throws IOException
	*/
	private static void checkCul(String[][] allArr, boolean[] flags,
					ArrayList<String> allStr, int[] lengths, BufferedReader ec,
					StringBuilder sb) throws IOException{

		String curCul = "";
		boolean firstTime = true;
		boolean flag = true;
		do{
			if(firstTime == true){
				allStr.add("");
				checkAna(allArr,flags,allStr,lengths,sb);
				allStr.remove(allStr.size()-1);

				curCul = ec.readLine();	
				firstTime = false;
			}
			if(curCul==null){
				ec = new BufferedReader(new FileReader("cullers.txt"));
				curCul = ec.readLine();
			}
			allStr.add(curCul);
			checkAna(allArr,flags,allStr,lengths, sb);
			allStr.remove(allStr.size()-1);		
		}while((curCul = ec.readLine()) != null);
	}
	/**
	* Checks the analysis array to see if there are any or not, 
	* and then calls the checkDis function to check for associated distance
	* methods.
	*
	* @param String[][] allArr - 2d array that contains all associated arrays
	* @param boolean[] flags - boolean array full of the associated flags
	* @param ArrayList<String> allStr - an arrayList containing all the 
	* 	possible combinations of strings
	* @param int[] lengths - an array full of the associated array lengths
	*/
	private static void checkAna(String[][] allArr, boolean[] flags, 
					ArrayList<String> allStr, int[] lengths,
					StringBuilder sb) throws IOException{
		boolean anaFlag = flags[2];
		int anaLength = lengths[2];
		if(anaFlag!=false){
			for(int j = 0; j<anaLength; j++){
				if(anaLength !=0 ) allStr.add(checkStr(allArr[2][j]));
				checkDis(allArr,flags,allStr,lengths,j, sb);
				allStr.remove(allStr.size()-1);
			}
		}else{ //No analysis
			checkDis(allArr,flags,allStr,lengths,-1,sb);
		}
	}
	/**
	* Checks the distance array to see if there are any or not, and finalizes
	* the rest of the process by calling printAll.
	* @param String[][] allArr - 2d array that contains all associated arrays
	* @param boolean[] flags - boolean array full of the associated flags
	* @param ArrayList<String> allStr - an arrayList containing all the 
	* 	possible combinations of strings
	* @param int[] lengths - an array full of the associated array lengths
	* @param int j - the index of the analysis that this distance is 
	* 	associated with
	*/
	private static void checkDis(String[][] allArr, boolean[] flags,	
					ArrayList<String> allStr, int[] lengths, int j, 
					StringBuilder sb) throws IOException{
		boolean disFlag = flags[3];
		int disLength = lengths[3];
		if(disFlag != false){
			if(j<disLength){
				allStr.add(allArr[3][j]);
				printAll(allStr, sb);
				allStr.remove(allStr.size()-1);
			}
			else 
				printAll(allStr, sb);
		}
		//No Distance
		else printAll(allStr, sb);
	}
	/**
	* Checks to see if an array is empty or not.
	* @param String[] arr - array to be checked
	* @return boolean related to if its empty or not.
	*/
	private static boolean checkArr(String[] arr){
		if(arr.length == 0)
			return false;
		return true;
	}
	/**
	* Turns null strings into empty strings
	* @param String s - string you're checking
	* @return the string passed or an empty string.
	*/
	private static String checkStr(String s){
		if(s == null){
			s = "";
		}
		return s;
	}
	/**
	* Creates the CSV file that contains all possible tests.
	* @param String[] can - String array containing all canonicizers
	* @param String[] dri - String array containing all event drivers
	* @param String[] cul - String array containing all event cullers
	* @param String[] ana - String array containing all analyses
	* @param String[] dis - String array containing all distance functions
	* @param StringBuilder sb - the string builder being passed to 
		the print function.
	* @throws IOException
	*/
	private static void createCSV(String[] can, String[] dri, String[] cul,
			String[] ana, String[] dis, StringBuilder sb)
			throws IOException{
		TestCombinations tc = new TestCombinations();
		
		System.out.println("Starting: Canonicizers");
		tc.createCombs("canon.txt",can);
		System.out.println("Canonicizers Done");
		
		System.out.println("Starting: Event Cullers");
		tc.createCombs("cullers.txt",cul);
		System.out.println("Event Cullers Done");
		
		BufferedReader ca = new BufferedReader(new FileReader("canon.txt"));
		BufferedReader ec = new BufferedReader(new FileReader("cullers.txt"));
		
		System.out.println("--------------Start Creation of CSV--------------");
		boolean firstTime = true;
		boolean culFlag = checkArr(cul);
		boolean driFlag = checkArr(dri);
		boolean anaFlag = checkArr(ana);
		boolean disFlag = checkArr(dis);
		
		String[][] allArr = {dri,cul,ana,dis};
		boolean[] flags = {driFlag,culFlag,anaFlag,disFlag};
		
		int[] lengths = {dri.length,cul.length,ana.length,dis.length};
		
		//Dri is 0. Cul is 1. Ana is 2. Dis is 3.
		
		String curCan = ""; String curCul = ""; String curDri = "";
		String curAna = ""; String curDis = "";
		
		for(int i=0; i<corpus.length;i++){
			ArrayList<String> allStr = new ArrayList<String>();
			do{
				if(firstTime == true){
					curCan = ca.readLine();
					curCan = checkStr(curCan);
					allStr.add(curCan);
				
					firstTime = false;
				}
				else allStr.add(checkStr(curCan));
			
				checkDri(allArr,flags,allStr,lengths,ec, sb);
				
				ec = new BufferedReader(new FileReader("cullers.txt"));
				allStr.remove(allStr.size()-1);
			
			}while((curCan = ca.readLine()) != null);
			ca = new BufferedReader(new FileReader("canon.txt"));
			firstTime=true;
			corCount++;
			nameCount=0;
		}
		ec.close();
		ca.close();
		if(sb.toString() != "" && sb.toString() != null){
			printWrite(sb,true);
		}
		System.out.println("--------------CSV has been Created--------------");
	}
	private static void rm(String cmd) throws Exception{
		String remove = "rm -r ";
		Process p = Runtime.getRuntime().exec(remove+cmd);
		p.waitFor();
	}
	private static void removeFiles() throws Exception{
		rm("canon.txt");
		rm("cullers.txt");
		rm("RunExperiment.csv");
		rm("Experiments");
	}
	/**
	* When a problem in the files being read occurs, like a mismatch, then it
	* tells the user which file is causing the error and then calls removeFiles.
	* There are three errors that this can print out:
	* "0" indicates a txt document is missing a correct topic header
	* "1" indicates a txt document is missing a correct language header
	* "2" indicates a general error, call with path = ""
	* @param String path - path to the file causing an error
	* @param int error - the case int, indicates the error intended, as listed 	
	*	above
	* @throws Exception
	*/
	private static void printError(String path,int error) throws Exception{
		switch(error){
			case 0:
				System.out.print("The file "+ path +
					" is missing a topic1 or topic2 declaration.");
				break;
			case 1:
				System.out.print("The file "+ path +
					" is missing an \"en\" or a \"gr\" declaration.");
				break;
			case 2:
				System.out.print("An error occured in the csv " +
					"file you specified.");
		}
		System.out.println(" Fix this and try again.");
		//removeFiles();
		System.exit(0);
	}
	/**
	* Starts running the experiments that were specified in the created CSV.
	* Also deletes the temporarily created files that were made earlier.
	* @param String pathJGAAP - path to the JGAAP.jar
	* @throws Exception
	*/
	private static void runExperiments(String pathJGAAP) throws Exception{
		for(int i = 0; i < sbTCount; i++){
			String csv = "./RunExps/RunExperiment"+(i+1)+".csv";
			try{		
				String cmd = "java -jar "+pathJGAAP +" -ee "+csv;
				System.out.println("JGAAP is now running on "+csv+".");
				Process p = Runtime.getRuntime().exec(cmd);
				p.waitFor();
				if(p.exitValue() == 0){	
					rm("canon.txt");
					rm("cullers.txt");
					System.out.println(csv+" is done!");				
				}
				else
					printError("",2);			
			}catch(Exception e){
				removeFiles();
				e.printStackTrace();
			}
		}
	}
	private static String[][] cusForEach(int i, String[][] arr1, String[] arr2){
		int j = 0;
		for(String x: arr2){
			arr1[i][j] = arr2[j];
			j++;
		}
		return arr1;
	}
	/**
	* Takes from the categories arrayList and adds to the associated
	* arrayList depending on what the index (if there is an event culler 
	* section in the csv, for example, then it will add it to the 
	* culler arraylist, if there isn't, then it'll just add a blank str)
	* @param int index - The point in the cat array that the string would be
	* @param ArrayList<String> arr - the associated arraylist
	* @param ArrayList<String> cat - category arraylist, containing the cells of 
	* 	the CSV that was read 
	*/
	private static void addList(int index, ArrayList<String> arr, ArrayList<String> cat){
		if(index != -1){ 
			String str = cat.get(index).trim();
			arr.add(str);
		}
		else arr.add("");
	}
	/**
	* Reads the CSV file from an associated path.
	* @param String path - a string that contains the path to the file.
	* @param boolean need2dArr - do you need a 2d array returned or not?
		if no, then returns an empty 2d array (still sets global vars)
	*/
    public static String[][] readCSV(String path, boolean need2dArr) throws FileNotFoundException{
        Scanner sc = new Scanner(new File(path));
        sc.useDelimiter(",");
        String str;
        
        int nIndex=-1, caIndex=-1, drIndex=-1,cuIndex=-1,
			anIndex=-1,diIndex=-1,coIndex=-1;
       	
		boolean firstTime = true;
        while((sc.hasNextLine())){
        	String temp = sc.nextLine();
				
        	if(firstTime == true){		
				ArrayList<String> categ = 
					new ArrayList<String>(Arrays.asList(temp.split(",")));
				nIndex = categ.indexOf("Names");
				caIndex = categ.indexOf("Canonicizers");
				drIndex = categ.indexOf("Event Drivers");
				cuIndex = categ.indexOf("Event Cullers");
				anIndex = categ.indexOf("Analyses");
				diIndex = categ.indexOf("Distance");
				coIndex = categ.indexOf("Corpus");
				firstTime = false;
        	}
        	else{
        		//Makes it so the split creates empty strings	
        		temp = temp.replaceAll(","," , "); 
				ArrayList<String> categ = 
					new ArrayList<String>(Arrays.asList(temp.split(",")));
					
				//Add each respective index to their list
        		addList(nIndex,nam,categ); addList(caIndex,can,categ);
        		addList(drIndex,dri,categ); addList(cuIndex,cul,categ);
        		addList(anIndex,ana,categ); addList(diIndex,dis,categ);
        		addList(coIndex,cor,categ);
        	}
        }

        sc.close();
        
		if(!need2dArr) return new String[1][1];
		else{
			String[][] temp = new String[6][nam.size()];
			temp = cusForEach(0,temp,createArr(nam));
			temp = cusForEach(1,temp,createArr(can));
			temp = cusForEach(2,temp,createArr(dri));
			temp = cusForEach(3,temp,createArr(cul));
			temp = cusForEach(4,temp,createArr(ana));
			temp = cusForEach(5,temp,createArr(dis));
			return temp;
		}
    }
    /**
    * Removes all empty and null strings from an arraylist and returns
    * a regular string array.
    * @param ArrayList<String> arr - The arraylist that is to 
    * 	be converted to an array
    * @return A string array
    */
	private static String[] createArr(ArrayList<String> arr){
		arr.removeAll(Collections.singleton(""));
		arr.removeAll(Collections.singleton(null));
		
		return arr.toArray(new String[arr.size()]);
	}
	//TODO: add extensions like AutomaticTest -gr to specify to run greek tests
		//and -en for english
	/**
	* Creates all possible combinations of corpora.
	* @param String path - path to the directory containing the txt files
	* @throws Exception
	*/
	public static void createCorpora(String path) throws Exception{
		File file = new File(path);
		File[] paths = file.listFiles();
		Arrays.sort(paths);
		int pathLen = paths.length;
		double trainSize = 0.75*pathLen;
		
		ArrayList<String> englishT1 = new ArrayList<String>();
		ArrayList<String> englishT2 = new ArrayList<String>();
		ArrayList<String> greekT1 = new ArrayList<String>();
		ArrayList<String> greekT2 = new ArrayList<String>();
	
		ArrayList<DualString> english = new ArrayList<DualString>();
		ArrayList<DualString> greek = new ArrayList<DualString>();
	
		for(int i=0;i<pathLen;i++){
			String textDoc = paths[i].toString();
			if(textDoc.contains("_en")){
				if(textDoc.contains("topic1"))
					englishT1.add(textDoc);
				else if(textDoc.contains("topic2"))
					englishT2.add(textDoc);
				else
					printError(textDoc,0);
			}
			else if(textDoc.contains("_gr")){
				if(textDoc.contains("topic1"))
					greekT1.add(textDoc);
				else if(textDoc.contains("topic2"))
					greekT2.add(textDoc);
				else
					printError(textDoc,0);
			}
			else
				printError(textDoc,1);
		}
		int et1 = englishT1.size(); int et2 = englishT2.size();
		int gr1 = greekT1.size(); int gr2 = greekT2.size();
		
		if((et1==et2) && (gr1==gr2)){
			int max = Math.max(et1, gr1);
			for(int i=0; i<max;i++){
				if(et1 != 0){
					String t1_en = englishT1.get(i);
					String t2_en = englishT2.get(i);			
					english.add(new DualString(t1_en,t2_en));
				}
				if(gr1 != 0){
					String t1_gr = greekT1.get(i);
					String t2_gr = greekT2.get(i);			
					greek.add(new DualString(t1_gr,t2_gr));
				}
			}
		}
		else{
			System.out.println("Error: There is an error in the specified "
				+"Corpus from the CSV file.  There is a mismatch in the amount"
				+" of English/Greek topic1s and topic2s.");
			System.out.println("Number of documents containing topic1_en: "
				+ et1);
			System.out.println("Number of documents containing topic2_en: "
				+ et2);
			System.out.println("Number of documents containing topic1_gr: "
				+ gr1);
			System.out.println("Number of documents containing topic2_gr: "
				+ gr2);
			System.exit(0);
		}
		TestCombinations tc = new TestCombinations();
		System.out.println("\nStarting: Corpora Creation");
		tc.createCorCsvs(greek);
		System.out.println("Corpora Creation Done");
	}
	public static void main(String[] args) throws Exception{
	
		File theDir = new File("./RunExps");
		if (!theDir.exists()) theDir.mkdir();
		System.out.print("Enter path to Experiment.csv: ");
		Scanner sc = new Scanner(System.in);
		String filePath = sc.nextLine();
		System.out.print("Enter path to JGAAP.jar: ");
		String jarPath = sc.nextLine();
		sc.close();
		sb = new StringBuilder();
		sb.append("RunExperiment\n");
		try{
			String[][] stuff = readCSV(filePath, false);
			String canon[] = createArr(can); 
			String driver[] = createArr(dri);
			String culler[] = createArr(cul); 
			String analys[] = createArr(ana);
			String dista[] = createArr(dis);
			
			cor.removeAll(Collections.singleton(""));
			cor.removeAll(Collections.singleton(null));
			
			for(String path: cor){
				createCorpora(path);	
			}	
				
			File file = new File("./Experiments");
			File[] fileList = file.listFiles();
			Arrays.sort(fileList);

			ArrayList<String> temp = new ArrayList<String>();	
			for(int i = 0; i<fileList.length;i++)
				temp.add(i,fileList[i].toString());
							
			corpus = createArr(temp);
			
			createCSV(canon,driver,culler,analys,dista, sb);
			runExperiments(jarPath);
			
		}catch(Exception e){
			removeFiles();
			e.printStackTrace();
		}
	}
}
