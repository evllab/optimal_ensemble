//Author: Nicholas Marshman
import java.util.ArrayList;
import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;

public class TestCombinations {
	private int count = 0;
	private int total = 0;

	/**
	* Recursive method, used to create all possible combinations of something.
	* Ex:  All combs of set {A,B} are A, B, and A&B.
	* @param String path - path to the file being created with all combinations
	* @param String[] arr - set of data you want to create combinations of
	* @throws IOException
	*/
	public void createCombs(String path, String[] arr) throws IOException{
		PrintWriter w = new PrintWriter(path);
		for(int i = 0; i < arr.length; i++){
			int len = i+1; //Length of the combinations
			if(len>3)
				break;
			String temp[]=new String[len];

			//Pass arr, the temporary combination array, 
			//the start and end of the loop, the current index of the 				//recursion, the length of the current combination and the
			//printwriter.  
			createCombs(arr, temp, 0, arr.length-1, 0, len, w);
			total += count;
			count = 0;
		}
		total=0;
		w.close();		
	}
	private void createCombs(String arr[], String temp[], 
				int start, int end,int ind, int len, PrintWriter w) 
				throws IOException {
		if(ind == len){ //we are at the length of the current combination
			String s = "";
			//Concat all current parts into one combination
			for (int i=0; i<len; i++){
				if(i!=(len-1)) s = s.concat(temp[i]+"&");
				else { 
					s = s.concat(temp[i]); 
					w.println(s);
					count++;
					return;
				}
			}
		}
		else{	
			for (int i=start; i<end+1; i++){
				if(!(end-i+1 >= len-ind)) break;
				temp[ind] = arr[i];
				createCombs(arr, temp, i+1, end, ind+1, len,w);
			}
		}
	}
	public void createCorCsvs(ArrayList<DualString> arr)
				throws IOException{
		String t1_a1; //topic 1 for the first author
		String t2_a1; //topic 2 for the first author
		String t1_a2; //topic 1 for the second author
		String t2_a2; //topic 2 for the second author

		File theDir = new File("./Experiments");
		if (!theDir.exists()) theDir.mkdir();

		int num = 1;
		for(int i = 0; i < arr.size()-1; i++){
			t1_a1 = arr.get(i).t1;
			t2_a1 = arr.get(i).t2;
			
			for(int j = i+1; j<arr.size();j++){
				t1_a2 = arr.get(j).t1;
				t2_a2 = arr.get(j).t2;
				
				createCSV(t1_a1,t1_a2,t2_a1,num++);
				//createCSV(t1_a1,t2_a2,t2_a1,num++);				
				createCSV(t1_a2,t1_a1,t2_a2,num++);						
				//createCSV(t1_a2,t2_a1,t2_a2,num++);							
				//createCSV(t2_a1,t1_a2,t1_a1,num++);	
				//createCSV(t2_a1,t2_a2,t1_a1,num++);			
				//createCSV(t2_a2,t1_a1,t1_a2,num++);				
				//createCSV(t2_a2,t2_a1,t1_a2,num++);	
			}	
		}		
	}
	private void createCSV(String auth1, String auth2, String unknown, int num)
				 throws IOException{
		String path = "Experiments/ExperimentCorpus"+num+".csv";
		PrintWriter pw = new PrintWriter(new File(path));		
		StringBuilder sb = new StringBuilder();
		
		sb.append("Correct,"+auth1+","+"Known Document\n");
		sb.append("Incorrect,"+auth2+","+"Known Document\n");
		sb.append(","+unknown+","+"Questioned Document\n");
		
		pw.write(sb.toString());
		pw.close();
	}
}
