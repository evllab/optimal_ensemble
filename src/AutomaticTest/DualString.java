public class DualString{
	String t1;
	String t2;
	
	public DualString(String t1, String t2){
		this.t1 = t1;
		this.t2 = t2;
	}
	public String toString(){
		return t1 + "," + t2;
	}
}
