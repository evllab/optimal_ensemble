ParserJGAAP
==

This includes the Java parser code for JGAAP test result .txt files. The parser is capable of parsing all of the .txt files recursively contained within a directory. The Parser will convert the data in the .txt files into separate .csv files in a separate directory specified by the user.
