/**
 * Given root directory, recursively finds all .txt files, parses them if JGAAP
 * result files and saves the parsed data into individual .csvs for each .txt.
 *
 * @author Derek S. Prijatelj
 */
package ParserJGAAP;

import java.util.LinkedList;
import java.io.File;
import java.io.IOException;

// TODO need to use gson library... or some means to output as JSON
// TODO add progress bar

public class RecursiveParser{

    private static LinkedList <File> recursiveGetTxtFiles(String pathToRootDir){
        return recursiveGetTxtFiles(new File(pathToRootDir));
    }

    /**
        Recursively obtains all .txt files from a root directory for parsing.
        Depth first search.

        @return LinkedList of Files ending with .txt
    */
    private static LinkedList <File> recursiveGetTxtFiles(File dir){
        LinkedList <File> txtFiles = new LinkedList<>();

        if (!dir.isDirectory()){
            return txtFiles; // this test is superfulous except first iter.
        }

        String[] contents = dir.list();
        File contentFile;

        for (String content : contents){
            contentFile = new File(dir.getPath() + "/"  + content);

            // If .txt file, add to list, if dir recurse, else ignore
            if (contentFile.isFile() && content.endsWith(".txt")){
                txtFiles.add(contentFile);
            } else if (contentFile.isDirectory()) {
                txtFiles.addAll(recursiveGetTxtFiles(contentFile));
            }
        }

        return txtFiles;
    }

    /**
        Parses the given File if a .txt file as a JGAAP test results file.
        Creates a corresponding csv at the specified root directory mimicing the
        .txt file's previous file path except starting at the root.
    */
    private static void parseTxtFile(File txtFile, File newRootDir){
        LogData logData = new LogData(txtFile); // parse log

        // ouput logData into csv at corresponding newRootDir.
        logData.createCSV(newRootDir);
    }

    private static void parseTxtFiles(LinkedList<File> files,
            String newRootDir){
        parseTxtFiles(files, new File(newRootDir));
    }
    /**
        Iterates through linked list to parse all .txt files as JGAAP test
        result files. If not JGAAP test results, then discards.
    */
    private static void parseTxtFiles(LinkedList<File> files, File newRootDir){
        for (File file : files){
            parseTxtFile(file, newRootDir);
        }
    }

    /*
        allows user to specify arguements, otherwise asks for the missing
        required arguements if not provided.

        The required arguements include:
        relative path to root directory of JGAAP result .txt files.
        relative path to new root directory for the .csv converted files.
    */
    public static void main(String[] args){
        assert(args.length == 2);
        String relPathJGAAPDir = args[0];
        String relPathNewDir = args[1];

        LinkedList<File> txtFiles = recursiveGetTxtFiles(relPathJGAAPDir);

        System.out.println("Finished Obtaining Files");
        System.out.println("txtFiles size = " + txtFiles.size());

        parseTxtFiles(txtFiles, relPathNewDir);
    }
}
