/**
 * Given JGAAP log file, parses all useful data from log. Parser is highly
 * dependent based on in text qualities of the log file.
 *
 * @author Derek S. Prijatelj
 */
package ParserJGAAP;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class LogData{
	private class Tuple{
		public final int rank;
		public final String author;
		public final double value;

		public Tuple(int rank, String author, double value){
			this.rank = rank;
			this.author = author;
			this.value = value;
		}
	}

	private class EventDriver{
		public final String name;
		private ArrayList<String> eventCullers;

		public EventDriver(String name){
			this.name = name;
			eventCullers = new ArrayList<>();
		}
		public EventDriver(String name, Collection<String> cullers){
			this.name = name;
			eventCullers = new ArrayList<>(cullers);
		}

        public String getEventCuller(int i){
            return eventCullers.get(i);
        }

        public int numEventCuller(){
            return eventCullers.size();
        }

        public boolean hasEventCuller(){
            return !eventCullers.isEmpty();
        }

        public void addEventCuller(String culler){
            eventCullers.add(culler);
        }
	}

	/**
	 * Stores all information of a single JGAAP test.
	 *
	 * TODO This object should be the one to process all the data from the file
	 * in its own constructors for each test. Then the LogData strings the tests
	 * together neatly in an ArrayList. If there is an empty line after every
	 * test, this should be implementable. This may not be necessary though,
	 * only prettier.
	 */
	private class TestData{
        // TODO make all of these private! Update code as necessary.
		private final String questionedDoc;
		private ArrayList<String> canonicizers;
		private ArrayList<EventDriver> eventDrivers;
		private ArrayList<String> analyses;
		private ArrayList<Tuple> results;

		public TestData(String docName){
			questionedDoc = docName;
			canonicizers = new ArrayList<>();
			eventDrivers = new ArrayList<>();
			analyses = new ArrayList<>();
			results = new ArrayList<>();
		}

        public String getQuestionedDoc(){
            return questionedDoc;
        }
        public String getCanonicizer(int i){
            return canonicizers.get(i);
        }
        public EventDriver getEventDriver(int i){
            return eventDrivers.get(i);
        }
        public String getAnalysis(int i){
            return analyses.get(i);
        }
        public Tuple getResult(int i){
            return results.get(i);
        }

        public int numCanonicizer(){
            return canonicizers.size();
        }
        public int numEventDriver(){
            return eventDrivers.size();
        }
        public int numAnalysis(){
            return analyses.size();
        }
        public int numResult(){
            return results.size();
        }

        public void addCanonicizer(String canonicizer){
            canonicizers.add(canonicizer);
        }
        public void addEventDriver(String name, Collection<String> cullers){
            this.addEventDriver(new EventDriver(name, cullers));
        }
        public void addEventDriver(EventDriver eventDriver){
            eventDrivers.add(eventDriver);
        }
        public void addAnalysis(String analysis){
            analyses.add(analysis);
        }
        public void addResult(int rank, String author, double value){
            this.addResult(new Tuple(rank, author, value));
        }
        public void addResult(Tuple tup){
            results.add(tup);
        }
	}

	public final String name; // Name of Log
	public final File originalFile;
	public ArrayList<TestData> tests = new ArrayList<>();

    public LogData(String logFilePath){
		//try{
            this(new File(logFilePath));
        //} catch (IOException e){
        //    e.printStackTrace();
        //}
	}

	/**
	 * Constructs LogData object from the given file.
	 *
	 * @param file String of file path to the log file
	 */
	public LogData(File logFile){
        name = logFile.getName().substring(0, logFile.getName().length() - 4);
        originalFile = logFile;

		try{
            if (!logFile.isFile() || !logFile.getName().endsWith(".txt"))
                throw new InvalidLogFileType();
            parse(logFile);
		} catch (InvalidLogFileType e){
			System.err.println("Error: '" + logFile.getName() + "' on path at '"
                + logFile.getPath() + "'"
                + " is not a valid log file type (should be .txt).\n"
                + "logFile.isFile() = " + Boolean.toString(logFile.isFile())
                + "\n"
                + "logFile.getName().endsWith(\".txt\") = "
                + Boolean.toString(logFile.getName().endsWith(".txt")));
			e.printStackTrace();
		}
	}

	/**
	 * Given a presumed log file, begins the parsing process
	 *
	 * @param logFile Log file to be parsed.
	 */
	private void parse(File logFile){
		try{
			Scanner sc = new Scanner(logFile);
			String line;

			// loops through all blank lines until all log tests processed
			while (sc.hasNextLine()){
				line = sc.nextLine().trim();
				if (!line.isEmpty() && line.contains(" ")){
                    //&& line.length() >= 3 // how is this necessary?

					tests.add(
                        new TestData(line.substring(line.lastIndexOf(' ') + 1))
                        );
					line = sc.nextLine().trim();

					if (line.equals("Canonicizers:") && sc.hasNextLine()){
						parseCanonicizer(sc, tests.get(tests.size() - 1));
					} else {
						sc.close();
						throw new InvalidLogStructure("Log incorrectly "
                        + "formatted. Expected 'Canonicizers', but found '"
                        + line + "'.");
					}
				} else if (!line.isEmpty()){
					sc.close();
					throw new InvalidLogStructure("First line is incorrectly "
                    + "formated");
				}
			}
			sc.close();
		} catch (IOException e){
			e.printStackTrace();
		} catch (InvalidLogStructure e){
			System.err.println("Error: Invalid Log Structure or Syntax in "
                + logFile.getPath());
			e.printStackTrace();
		} catch (InvalidResultValue e){
            System.err.println("Error: The log: '" + logFile.getPath()
                + "' contains test results with NaN or Infinite as an "
                + "evaluation score.");
			e.printStackTrace();
            // May want to make this a warning, rather than an Error.
        }
	}

	/**
	 * Parses the Canonicizer part of the current test in the log file. Updates
	 * the current TestData object in tests.
	 *
	 * @param sc Scanner used to read the log file
	 * @param test TestData object being added to
	 */
	private void parseCanonicizer(Scanner sc, TestData test)
            throws InvalidLogStructure, InvalidResultValue{
		String line = (sc.nextLine()).trim();

		while (!line.equals("EventDrivers:")){
			test.canonicizers.add(line);
			if (sc.hasNextLine())
				line = (sc.nextLine()).trim();
			else {
				sc.close();
				throw new InvalidLogStructure("Log ends early while parsing "
                + "Canonicizers.");
			}
		}

		if (sc.hasNextLine()){
			parseEventDrivers(sc, test);
		} else {
			sc.close();
			throw new InvalidLogStructure("Log ends early while beginning to "
            + "parse Event Drivers.");
		}
	}

	private static boolean isEventCuller(String line){
		return !(line.trim().equals("Analysis:"))
            && line.length() >= 9
            && !(line.substring(9)).isEmpty()
            && (line.substring(9)).charAt(0) == ' ';
	}

	/**
	 * Parses the EventDriver part of the current test in the log file. Updates
	 * the current TestData object in tests.
	 *
	 * @param sc Scanner used to read the log file
	 * @param test TestData object being added to
	 *
     * TODO: Appropriately handle the hierarchy of data of Event Handlers
     */
	private void parseEventDrivers(Scanner sc, TestData test)
            throws InvalidLogStructure, InvalidResultValue{
		// Uses the actual number of spaces to determine hierarchy. Do not trim.
		String line = sc.nextLine();

		while (!(line.trim().equals("Analysis:"))){
			if (sc.hasNextLine()){ // Event Driver
				EventDriver eventDriver = new EventDriver(line.trim());

				if (sc.hasNextLine()){
					line = sc.nextLine();

					while (isEventCuller(line)){// Event Culler
						if (line.length() >= 17){
							line = line.substring(17);
							if (line.charAt(0) == ' '){ // Specific Event Culler
								eventDriver.addEventCuller(line.trim());
							}
						}
						if (sc.hasNextLine()){
							line = sc.nextLine();
						} else {
							throw new InvalidLogStructure("Log ends early "
                            + "while parsing Event Culler.");
						}
					}
				} else {
					throw new InvalidLogStructure("Log ends early while "
                    + "parsing Event Drivers.");
				}
				test.eventDrivers.add(eventDriver);

			} else {
				System.out.println("line = " + line);
				sc.close();
				throw new InvalidLogStructure("Log ends early while parsing "
                + "Event Drivers.");
			}
		}

		if (line.trim().equals("Analysis:") && sc.hasNextLine()){
			parseAnalysis(sc, test);
		} else {
			sc.close();
			throw new InvalidLogStructure("Log ends early or incorrect "
            + "formatting where 'Analysis:' was expected, but '" + line
            + "' was found.");
		}
	}

	/**
	 * Parses the Analysis part of the current test in the log file. Updates the
	 * current TestData object in tests.
	 *
	 * @param sc Scanner used to read the log file
	 * @param test TestData object being added to
	 */
	private void parseAnalysis(Scanner sc, TestData test)
            throws InvalidLogStructure, InvalidResultValue{
		String line = (sc.nextLine()).trim();

		while (!(line.length() >= 2 && (line.substring(0, 2)).equals("1."))){
			test.addAnalysis(line);
			if (sc.hasNextLine())
				line = (sc.nextLine()).trim();
			else {
				sc.close();
				throw new InvalidLogStructure("Log ends early while parsing "
                + "Analyses.");
			}
		}

		if (line.length() >= 2 && (line.substring(0, 2)).equals("1.")){
			parseResults(sc, test, line);
		} else {
			sc.close();
			throw new InvalidLogStructure("Log formatted incorrectly. Expected "
            + "first result, but found '" + line + "'.");
		}
	}

	/**
	 * Parses the analyzed results of the current test in the log file. Updates
	 * the current TestData object in tests. The results are in the order
	 * presented in the file.
	 *
	 * @param sc Scanner used to read the log file
	 * @param test TestData object being added to
	 */
	private void parseResults(Scanner sc, TestData test, String line)
            throws InvalidLogStructure, InvalidResultValue{
		String[] strArr;
		do{
			// tokenize the content. 2nd token is author name, 3rd prob value
			strArr = line.split(" ");

            if (strArr.length != 3){
                throw new InvalidLogStructure("Log result improperly "
                + "formatted. Expected 3 strings separated by a whitespace.");
            }

			if (strArr[2].equals("NaN") || strArr[2].equals("Infinity")){
				// TODO Check if strArr[2] result value is NaN, Throw Exception!
				throw new InvalidResultValue();
			}

			test.addResult(
                Integer.parseInt(
                    strArr[0].substring(0, strArr[0].length() - 1)),
                strArr[1],
                Double.parseDouble(strArr[2])
                );

			if (sc.hasNextLine())
				line = (sc.nextLine()).trim();
			else
				break;
		} while (!line.isEmpty());
	}

	/**
	    Prints out the entire Log's data in order, as seen in the format of the
	    actual log.

        TODO needs testing!!!! may not be correct!
	 */
	public void print(){
		System.out.println("\nLog: " + name);
		for (int i = 0; i < tests.size(); i++){
			System.out.println("\nQuestioned Document: "
                + tests.get(i).questionedDoc);

			System.out.println("Canonicizers:");
			for (int j = 0; j < tests.get(i).numCanonicizer(); j++){
				System.out.println("\t" + tests.get(i).getCanonicizer(j));
			}

			System.out.println("EventDrivers:");
			for (int j = 0; j < tests.get(i).numEventDriver(); j++){
				System.out.println("\t" + tests.get(i).getEventDriver(j).name);
				if (tests.get(i).getEventDriver(j).hasEventCuller()){
					System.out.println("\t\tEventCullers:");
				}
				for (int k = 0;
                     k < tests.get(i).getEventDriver(j).numEventCuller();
                     k++){
					System.out.println("\t\t\t"
                        + tests.get(i).getEventDriver(j).getEventCuller(k));
				}
			}

			System.out.println("Analysis:");
			for (int j = 0; j < tests.get(i).numAnalysis(); j++){
				System.out.println("\t" + tests.get(i).getAnalysis(j));
			}

			for (int j = 0; j < tests.get(i).results.size(); j++){
				System.out.println((j + 1) + ". "
                    + tests.get(i).getResult(j).author + " "
                    + tests.get(i).getResult(j).value);
			}
		}
	}

    /**
        Creates a .csv file containing the content of the LogData Object at the
        given directory. The header fields are questioned doc and specific
        analysis method, File, value. This makes a csv with 3 columns. For the
        first column header, the individual parts are tab delimited and multiple
        parts of the same type are underscore delimited. EvenCullers are hyphen
        delimited.

        @param dir root directory for the createCSV to go relative to program
            location of execution.
    */
    public void createCSV(File dir){
        // create dir if it does not already exist
        if (!dir.exists()){
            dir.mkdir();
        }

        // remove all part of the path before the first canonicizer
        String path = originalFile.getPath();

        // remove all leading "../"
        while (path.substring(0,3).equals("../")){
            path = path.substring(3);
        }

        path = dir.getPath() + "/" + path.substring(0, path.length() - 4) + "/";

        // make the dir of LogData, then loop and make csvs for all TestDatas
        File newPath = new File(path);
        newPath.mkdirs();

        String qdoc;
        for (TestData test : tests){
            try{
                qdoc = "qdoc" + test.questionedDoc.replaceAll(
                    File.separator, "-");
                qdoc = qdoc.substring(0, qdoc.length() - 4) + ".csv";
                PrintWriter pw = new PrintWriter(path + qdoc, "UTF-8");

                String header = test.questionedDoc + "\t";

                for (int i = 0; i < test.numCanonicizer(); i++){
                    header += test.getCanonicizer(i).replaceAll(',','.');
                    if (i > 0 && i < test.numCanonicizer() - 1){
                        header += "_";
                    }
                }

                header += "\t";

                for (int i = 0; i < test.numEventDriver(); i++){
                    header += test.getEventDriver(i).name.replaceAll(',','.');

                    if (test.getEventDriver(i).hasEventCuller()){
                        for (int j = 0;
                             j < test.getEventDriver(i).numEventCuller();
                             j++){
                             header += test.getEventDriver(i).getEventCuller(j);
                             if (j > 0
                                 && j <
                                    test.getEventDriver(i).numEventCuller() - 1
                                ){
                                header += "`";
                             }
                         }
                    }

                    if (i > 0 && i < test.numEventDriver() - 1){
                        header += "_";
                    }
                }

                header += "\t";

                for (int i = 0; i < test.numAnalysis(); i++){
                    header += test.getAnalysis(i);
                    if (i > 0 && i < test.numAnalysis() - 1){
                        header += "_";
                    }
                }

                pw.println(header + ",File,Value");

                // Now print results Tuples: rank, author, value

                for (Tuple result : test.results){
                    pw.println(result.rank + ","
                               + result.author + ","
                               + result.value);
                }

                pw.close();
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    /**
        Creates a json file containing the content of the LogData Object at the
        given directory.
    */
    public void createJSON(File dir){

    }

    private class InvalidLogFileType extends Exception{
        public InvalidLogFileType () {}

        public InvalidLogFileType (String message){
            super (message);
        }

        public InvalidLogFileType (Throwable cause){
            super (cause);
        }

        public InvalidLogFileType (String message, Throwable cause){
            super (message, cause);
        }
    }

    private class InvalidLogStructure extends Exception{
        public InvalidLogStructure () {}

        public InvalidLogStructure (String message){
            super (message);
        }

        public InvalidLogStructure (Throwable cause){
            super (cause);
        }

        public InvalidLogStructure (String message, Throwable cause){
            super (message, cause);
        }
    }

    private class InvalidResultValue extends Exception{
        public InvalidResultValue () {}

        public InvalidResultValue (String message){
            super (message);
        }

        public InvalidResultValue (Throwable cause){
            super (cause);
        }

        public InvalidResultValue (String message, Throwable cause){
            super (message, cause);
        }
    }
}
